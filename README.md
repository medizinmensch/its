# IT Security

Dies sind die Übungsaufgaben für das Modul IT Security im Master Angewandte Informatik an der HTW Berlin.

Die Aufgaben wurden bearbeitet von Oskar Sailer (s0553791) und Youri Seichter (s0553414).

## Konfiguration

Wir haben mit Java 8 gearbeitet und die Tests mit JUnit4.13 und JUnit-Jupiter 5.4 ausgeführt. 

Wir nutzten IntelliJ als Entwicklerumgebung und konnten so Tests einzeln starten und haben die Konsolenausgabe zum debuggen genutzt.
Das Projekt nutzt allerdings auch Maven wodurch der Buildprozess auch über die Kommandozeile ausgeführt werden kann.   

Zum Bauen des Projektes:
```bash
mvn clean compile
```

Wenn auch mit Intellij gearbeitet werden soll, sollten die Ordner in den Modul-Einstellungen wie folgt festgelegt werden:

* `./tasks/src/main/java` als "source directory"
* `./tasks/src/main/resources` als "resources directory"
* `./tasks/src/test/java` als "test source directory"
* `./tasks/src/test/resources` als "test resources directory"

## Abgaben

Alle fünf Aufgaben wurden gelößt, in einem separaten Branch bearbeitet und zusätzlich in den master merged.

Für jede Abgabe wurde ein [merge request](https://gitlab.com/medizinmensch/its/-/merge_requests) angelegt, der nur dazu dient, die Differenz zur vorherigen Abgabe abzubilden. 
Außerdem gibt es für jede Abgabe ein HTML- und PDF-Dokument, welches die Testergebnisse präsentiert.   

### 1 BigInt
* [Branch - BigInt](https://gitlab.com/medizinmensch/its/-/tree/1_BigInt)
* [Diff von Aufgabe 1](https://gitlab.com/medizinmensch/its/-/merge_requests/5/diffs)
* Zusätzliche Notizen liegen unter      ```src/main/resources/notes-for-task1.txt```
* Command, um nur die Tests für Aufgabe 1 auszuführen:
```bash
mvn clean test -Dtest=ArithmeticTests,CompareTests,ConvertHexTests,Div10Tests,Mul10tests,ShiftLeftTests,ShiftRightTests
```

### 2 Primzahlen
* [Branch - Primzahlen](https://gitlab.com/medizinmensch/its/-/tree/2_Primzahlen)
* [Diff von Aufgabe 2](https://gitlab.com/medizinmensch/its/-/merge_requests/4/diffs)
* Zusätzliche notizen liegen unter      ```src/main/resources/notes-for-task2.txt```
* Die zwei Fragen aus dem Aufgabenblatt, sowie die damit verbundenen empirisch ausgeführten Analysen sind hier festgehalten:    ```src/main/resources/prime-test-results.txt```
* Unter ```src/main/resources/``` wurden weitere txt-Dateien erstellt, die Primzahlen oder Pseudo-Primzahlen enthalten, die für die Primzahlgenerierung und die Tests benutzt werden.
* Command, um nur die Tests für Aufgabe 2 auszuführen:      
```bash
mvn clean test -Dtest=EulerPseudoTests,FermatPseudoTests,GcdTests,NoPrimeNumberTests,PowerBigTests,PowerModPrimTests,PowerModTests,PowerSmallTests,PrimeNumberTests,SquareTests
```

### 3 RSA
* [Branch - RSA Verschlüsselung](https://gitlab.com/medizinmensch/its/-/tree/3_RSA)
* [Diff von Aufgabe 3](https://gitlab.com/medizinmensch/its/-/merge_requests/3/diffs)
* Zusätzliche notizen liegen unter      ```src/main/resources/notes-for-task3.txt```
* Command, um nur die Tests für Aufgabe 3 auszuführen:
```bash
mvn clean test -Dtest=EgcdTests,RSATests1,RSATests2,RSATests3
```

### 4 BBS
* [Branch - Blum-Blum-Shub](https://gitlab.com/medizinmensch/its/-/tree/4_BBS)
* [Diff von Aufgabe 4](https://gitlab.com/medizinmensch/its/-/merge_requests/2/diffs)
* Zusätzliche notizen liegen unter      ```src/main/resources/notes-for-task4.txt```
* Die statistische Analyse wurde durchgeführt. Die Ergebnisse sind in den beiden Dateien ```bbs_256_10000_randomNumbers.txt``` & ```bbs_65536_1000000_randomNumbers.txt``` dokumentiert, die unter ```src/main/resources/``` liegen.
* Command, um nur die Tests für Aufgabe 4 auszuführen:
```bash
mvn clean test -Dtest=BBSTests,BBS_findPTests,BBS_findPAndQTests
```

### 5 SHA
* [Branch - Sha256](https://gitlab.com/medizinmensch/its/-/tree/5_sha256)
* [Diff von Aufgabe 5](https://gitlab.com/medizinmensch/its/-/merge_requests/6/diffs)
* Aufgrund der verzweiten Datenstruktur wurde für diese Tests der gesonderte ShaTestFileReader genutzt.   
* Command, um nur die Tests für Aufgabe 5 auszuführen:    
```bash
mvn clean test -Dtest=ShaTests
```

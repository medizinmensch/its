package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class PowerModTests {

    final private String PATH = "/primzahlen";
    final private String FILENAME = "/PowerMod-Tests.txt";
    final private String NUMBER_INDEX = "abcdefghi";

    @Test
    public void testPowerMod() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt a = BigInt.toBigIntFromHex(currentTest.get("a"));
            BigInt b = BigInt.toBigIntFromHex(currentTest.get("b"));
            BigInt m = BigInt.toBigIntFromHex(currentTest.get("m"));
            BigInt comparison = BigInt.toBigIntFromHex(currentTest.get("c"));
            BigInt result = BigInt.powerMod(a, b, m);

            // result.debugString();
            Assert.assertEquals("a^b mod m does not equal comparison: " + currentTest.get("c"), BigInt.compare(result, comparison), 0);
        }
    }
}

package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class RSATests3 {

    final private String PATH = "/rsa";
    final private String FILENAME = "/RSA-Tests-3.txt";
    final private String NUMBER_INDEX = "abcdefghi";
    // final private String decryptDoesNotFail = 3-1 & 3-2

    @Test
    public void testRSAWrongEncrypt() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));

            // key generation
            BigInt a = BigInt.toBigIntFromHex(currentTest.get("a"));
            BigInt b = BigInt.toBigIntFromHex(currentTest.get("b"));
            BigInt p = new BigInt();
            BigInt.mul(a, b, p);
            BigInt pComparison = BigInt.toBigIntFromHex(currentTest.get("p"));

            Assert.assertEquals("Calculated p does not equal comparison" + currentTest.get("P"), BigInt.compare(p, pComparison), 0);

            BigInt q = BigInt.toBigIntFromHex(currentTest.get("q"));
            BigInt e = BigInt.toBigIntFromHex(currentTest.get("e"));

            BigInt phiComparison = BigInt.toBigIntFromHex(currentTest.get("f"));
            BigInt dComparison = BigInt.toBigIntFromHex(currentTest.get("d"));

            RSAKeys keys = RSAKeys.generateRSAKeys(e, 0, p, q);
            
            Assert.assertEquals("private key 'd' should be " + currentTest.get('D'), BigInt.compare(keys.getPrivateKey().getD(), dComparison), 0);
            Assert.assertEquals("phi should be " + currentTest.get('F'), BigInt.compare(keys.getPhi(), phiComparison), 0);

            // encryption
            BigInt plain;
            BigInt cypher;
            BigInt cypherCompare;

            plain = BigInt.toBigIntFromHex(currentTest.get("g"));
            cypherCompare = BigInt.toBigIntFromHex(currentTest.get("h"));
            cypher = RSA.encryptRSA(keys.getPublicKey(), plain);
            Assert.assertEquals("Encrypted value does not match comparison " + currentTest.get("H"), BigInt.compare(cypher, cypherCompare), 0);

            plain = BigInt.toBigIntFromHex(currentTest.get("i"));
            cypherCompare = BigInt.toBigIntFromHex(currentTest.get("j"));
            cypher = RSA.encryptRSA(keys.getPublicKey(), plain);
            Assert.assertEquals("Encrypted value does not match comparison " + currentTest.get("J"), BigInt.compare(cypher, cypherCompare), 0);

            plain = BigInt.toBigIntFromHex(currentTest.get("k"));
            cypherCompare = BigInt.toBigIntFromHex(currentTest.get("l"));
            cypher = RSA.encryptRSA(keys.getPublicKey(), plain);
            Assert.assertEquals("Encrypted value does not match comparison " + currentTest.get("L"), BigInt.compare(cypher, cypherCompare), 0);

        }
    }

    @Test
    public void testRSAWrongDecrypt() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            int testNumber = Integer.parseInt(testName.split("-")[1]);
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));

            // key generation
            BigInt a = BigInt.toBigIntFromHex(currentTest.get("a"));
            BigInt b = BigInt.toBigIntFromHex(currentTest.get("b"));
            BigInt p = new BigInt();
            BigInt.mul(a, b, p);
            BigInt pComparison = BigInt.toBigIntFromHex(currentTest.get("p"));

            Assert.assertEquals("Calculated p does not equal comparison" + currentTest.get("P"), BigInt.compare(p, pComparison), 0);

            BigInt q = BigInt.toBigIntFromHex(currentTest.get("q"));
            BigInt e = BigInt.toBigIntFromHex(currentTest.get("e"));

            BigInt dComparison = BigInt.toBigIntFromHex(currentTest.get("d"));

            RSAKeys keys = RSAKeys.generateRSAKeys(e, 0, p, q);

            Assert.assertEquals("private key 'd' should be " + currentTest.get('D'), BigInt.compare(keys.getPrivateKey().getD(), dComparison), 0);

            BigInt plain;
            BigInt cypher;
            BigInt plainCompare;

            // expecting a non matching decrypted value
            // excluding test cases 1-1, 1-2, 1-3 & 3-1, 3-2 because decryption works for these test cases
            if (testNumber != 1 && testNumber != 3) {
                cypher = BigInt.toBigIntFromHex(currentTest.get("h"));
                plainCompare = BigInt.toBigIntFromHex(currentTest.get("g"));
                plain = RSA.decryptRSA(keys.getPrivateKey(), cypher);
                Assert.assertNotEquals("Decrypted value matches comparison " + currentTest.get("G") + " but should not", BigInt.compare(plain, plainCompare), 0);

                cypher = BigInt.toBigIntFromHex(currentTest.get("j"));
                plainCompare = BigInt.toBigIntFromHex(currentTest.get("i"));
                plain = RSA.decryptRSA(keys.getPrivateKey(), cypher);
                Assert.assertNotEquals("Decrypted value matches comparison " + currentTest.get("I") + "but should not", BigInt.compare(plain, plainCompare), 0);

                cypher = BigInt.toBigIntFromHex(currentTest.get("l"));
                plainCompare = BigInt.toBigIntFromHex(currentTest.get("k"));
                plain = RSA.decryptRSA(keys.getPrivateKey(), cypher);
                Assert.assertNotEquals("Decrypted value matches comparison " + currentTest.get("K") + "but should not", BigInt.compare(plain, plainCompare), 0);
            } else if (testNumber == 3) {
                cypher = BigInt.toBigIntFromHex(currentTest.get("h"));
                plainCompare = BigInt.toBigIntFromHex(currentTest.get("g"));
                plain = RSA.decryptRSA(keys.getPrivateKey(), cypher);
                Assert.assertEquals("Decrypted value does not match comparison " + currentTest.get("G") + " but should not", BigInt.compare(plain, plainCompare), 0);

                cypher = BigInt.toBigIntFromHex(currentTest.get("j"));
                plainCompare = BigInt.toBigIntFromHex(currentTest.get("i"));
                plain = RSA.decryptRSA(keys.getPrivateKey(), cypher);
                Assert.assertEquals("Decrypted value does not match comparison " + currentTest.get("I") + "but should not", BigInt.compare(plain, plainCompare), 0);

                cypher = BigInt.toBigIntFromHex(currentTest.get("l"));
                plainCompare = BigInt.toBigIntFromHex(currentTest.get("k"));
                plain = RSA.decryptRSA(keys.getPrivateKey(), cypher);
                Assert.assertNotEquals("Decrypted value matches comparison " + currentTest.get("K") + "but should not", BigInt.compare(plain, plainCompare), 0);
            }
        }
    }
}

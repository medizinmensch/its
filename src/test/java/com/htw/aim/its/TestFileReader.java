package com.htw.aim.its;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TestFileReader {
    public static ArrayList<HashMap<String, String>> getTestsFromFileAsList(String testFilePath) throws Exception {

        ReportGenerator.writeHeader(testFilePath.substring(1,testFilePath.length()-4), 1);
        ArrayList<HashMap<String, String>> testList = new ArrayList<>();

        // read file
        Class clazz = TestFileReader.class;
        InputStream inputStream = clazz.getResourceAsStream(testFilePath);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;

            //System.out.println("Finished skipping comments.");

            // line by line
            HashMap<String, String> currentTestCase = new HashMap<>();
            while ((line = br.readLine()) != null) {
                if (line.equals(""))
                    continue;

                // skip comments
                if (line.charAt(0) == '#') {
                    // reached end of current test
                    if (line.length() > 1 && line.charAt(1) == '-' && currentTestCase.containsKey("t")) {
                        testList.add(currentTestCase);
                        currentTestCase = new HashMap<>();
                    }
                } else {
                    String[] lineArray = line.split("=");
                    if (lineArray.length > 1) {
                        currentTestCase.put(lineArray[0], lineArray[1]);
                    } else {
                        currentTestCase.put(lineArray[0], null);
                    }
                }

            }
        }
        return testList;
    }
}

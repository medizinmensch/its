package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class PowerBigTests {

    final private String PATH = "/primzahlen";
    final private String FILENAME = "/Power-Big-Tests.txt";
    final private String NUMBER_INDEX = "abcdefghi";

    @Test
    public void testPowerBig() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt a = BigInt.toBigIntFromHex(currentTest.get("a"));
            BigInt b = BigInt.toBigIntFromHex(currentTest.get("b"));

            // at test 9 reading bigint from hex value fails?!
            BigInt comparison = new BigInt(currentTest.get("C"));
            BigInt result = BigInt.power(a, b);

            Assert.assertEquals("a^b does not equal comparison: " + currentTest.get("C"), BigInt.compare(result, comparison), 0);
        }
    }
}

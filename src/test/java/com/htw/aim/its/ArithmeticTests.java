package com.htw.aim.its;

import com.htw.aim.its.BigInt;
import com.htw.aim.its.BigIntConfig;
import com.htw.aim.its.TestFileReader;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.HashMap;

import static com.htw.aim.its.ReportGenerator.*;

public class ArithmeticTests {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    final private String PATH = "/bigint";
    final private String FILENAME = "/Arithmetic-Tests.txt";

    @Test
    public void testAddition() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);

        writeHeader("Addition", 2);
        startCodeSection();
        for (HashMap<String, String> currentTest : testList) {
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt a = new BigInt(currentTest.get("A"));
            BigInt b = new BigInt(currentTest.get("B"));
            BigInt result = new BigInt(0);
            BigInt.add(a, b, result);
            BigInt comparison = new BigInt(currentTest.get("C"));

            printTestResult("+", currentTest.get("t"), a.toDecimalString(), b.toDecimalString(), result.toDecimalString(), comparison.toDecimalString());
            Assert.assertEquals("Result was " + result + " but should be " + currentTest.get("C"), BigInt.compare(result, comparison), 0);
        }
        printFinalSection();
    }

    @Test
    public void testSubtraction() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);

        writeHeader("Subtraction", 2);
        startCodeSection();
        for (HashMap<String, String> currentTest : testList) {
            BigInt a = new BigInt(currentTest.get("A"));
            BigInt b = new BigInt(currentTest.get("B"));
            BigInt result = new BigInt(0);
            BigInt.sub(a, b, result);
            BigInt comparison = new BigInt(currentTest.get("D"));

            printTestResult("-", currentTest.get("t"), a.toDecimalString(), b.toDecimalString(), result.toDecimalString(), comparison.toDecimalString());
            Assert.assertEquals("Result was " + result + " but should be " + currentTest.get("D"), BigInt.compare(result, comparison), 0);
        }
        printFinalSection();
    }

    @Ignore
    @Test
    public void testMultiplication() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);

        writeHeader("Multiplication", 2);
        startCodeSection();
        for (HashMap<String, String> currentTest : testList) {
            BigInt a = new BigInt(currentTest.get("A"));
            BigInt b = new BigInt(currentTest.get("B"));
            BigInt result = new BigInt(BigIntConfig.MAX_SIZE, 0);
            BigInt.mul(a, b, result);
            BigInt comparison = new BigInt(currentTest.get("E"));

            printTestResult("*", currentTest.get("t"), a.toDecimalString(), b.toDecimalString(), result.toDecimalString(), comparison.toDecimalString());
            Assert.assertEquals("Result was " + result + " but should be " + currentTest.get("E"), BigInt.compare(result, comparison), 0);
        }
        printFinalSection();
    }

    @Ignore
    @Test
    public void testDivision() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);

        writeHeader("Division", 2);
        startCodeSection();
        for (HashMap<String, String> currentTest : testList) {
            BigInt a = new BigInt(currentTest.get("A"));
            BigInt b = new BigInt(currentTest.get("B"));
            BigInt result = new BigInt(0);

            String expectedResult = currentTest.get("F");
            // expect division by 0 --> exception thrown
            if (expectedResult == null) {
                Assertions.assertThrows(Exception.class, () -> BigInt.divmod(a, b, result));
                exception.expect(Exception.class);
                System.out.print(String.format("%s successfully catched exception (can't divide by zero)\n", currentTest.get("t")));
            } else {
                BigInt.divmod(a, b, result);
                BigInt comparison = new BigInt(currentTest.get("F"));
                printTestResult("/", currentTest.get("t"), a.toDecimalString(), b.toDecimalString(), result.toDecimalString(), comparison.toDecimalString());

                Assert.assertEquals("Result was " + result + " but should be " + expectedResult, BigInt.compare(result, comparison), 0);
            }
        }
        printFinalSection();
    }

    @Ignore
    @Test
    public void testModulo() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);

        writeHeader("Modulo", 2);
        startCodeSection();
        for (HashMap<String, String> currentTest : testList) {
            BigInt a = new BigInt(currentTest.get("A"));
            BigInt b = new BigInt(currentTest.get("B"));
            BigInt result = new BigInt(0);
            String expectedResult = currentTest.get("G");

            if (expectedResult == null) {
                Assertions.assertThrows(Exception.class, () -> BigInt.divmod(a, b, result));
                exception.expect(Exception.class);
            } else {
                BigInt modulo = BigInt.divmod(a, b, result);
                BigInt comparison = new BigInt(expectedResult);

                printTestResult("%", currentTest.get("t"), a.toDecimalString(), b.toDecimalString(), modulo.toDecimalString(), comparison.toDecimalString());
                Assert.assertEquals("Result was " + modulo + " but should be " + expectedResult, BigInt.compare(modulo, comparison), 0);
            }
        }
        printFinalSection();
    }
}

package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class PowerSmallTests {

    final private String PATH = "/primzahlen";
    final private String FILENAME = "/Power-Small-Tests.txt";
    final private String NUMBER_INDEX = "abcdefghi";

    @Test
    public void testPowerSmall() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt a = BigInt.toBigIntFromHex(currentTest.get("a"));
            BigInt result;
            BigInt comparison;

            for (int i = 0; i < 8; i++) {
                result = BigInt.power(a, new BigInt(i));
                String currentChar = NUMBER_INDEX.charAt(i+1) + "";
                comparison = BigInt.toBigIntFromHex(currentTest.get(currentChar));
                Assert.assertEquals("result^" + i + " does not equal comparison: " + currentTest.get(currentChar), BigInt.compare(result, comparison), 0);
            }
        }
    }
}

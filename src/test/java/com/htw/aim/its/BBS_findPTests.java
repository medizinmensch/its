package com.htw.aim.its;

import com.htw.aim.its.domain.BBS;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class BBS_findPTests {

    final private String PATH = "/bbs";
    final private String FILENAME = "/Finden eines p.txt";

    @Test
    public void findP() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt startValue = BigInt.toBigIntFromHex(currentTest.get("a"));
            BigInt comparison;

            for (int i = 1; i < 11; i++) {
                BigInt prime = BBS.findPrimeForBBS(startValue);

                comparison = BigInt.toBigIntFromHex(currentTest.get("p" + i));
                System.out.println("P for iteration " + i + ": " + prime);
                Assert.assertEquals("Found p for BBS should be: " + currentTest.get("P" + i), BigInt.compare(prime, comparison), 0);

                startValue = prime;
            }
        }
    }
}

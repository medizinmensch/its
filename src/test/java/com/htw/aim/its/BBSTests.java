package com.htw.aim.its;

import com.htw.aim.its.domain.BBS;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class BBSTests {

    final private String PATH = "/bbs";
    final private String FILENAME = "/BBS-Tests.txt";

    @Test
    public void bbsTests() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt p = BigInt.toBigIntFromHex(currentTest.get("p"));
            BigInt q = BigInt.toBigIntFromHex(currentTest.get("q"));
            BigInt seed = BigInt.toBigIntFromHex(currentTest.get("r"));
            BBS currentBBS = new BBS(seed, p, q);
            BigInt comparison;

            String byteValue = currentBBS.getNextByte();
            BigInt[] firstEightSeeds = currentBBS.getLastSeeds();
            byteValue += currentBBS.getNextByte();
            BigInt[] lastEightSeeds = currentBBS.getLastSeeds();

            // print all seeds of 16 bit generation
            //for (int i = 0; i < 16; i++) {
            //    if (i < 8)
            //        System.out.print(firstEightSeeds[i] + ", ");
            //    else
            //        System.out.print(lastEightSeeds[i-8] + ", ");
            //}
            //System.out.println();


            // compare seed of each step here in two batches
            for (int i = 1; i < 9; i++) {
                comparison = BigInt.toBigIntFromHex(currentTest.get("z" + i));
                // System.out.println("Calculated Z for iteration " + i + ": " + firstEightSeeds[i-1]);
                Assert.assertEquals("Seed of iteration " + i + " should be: " + currentTest.get("Z" + i), BigInt.compare(firstEightSeeds[i-1], comparison), 0);
            }
            for (int i = 9; i < 17; i++) {
                comparison = BigInt.toBigIntFromHex(currentTest.get("z" + i));
                // System.out.println("Calculated Z for iteration " + i + ": " + lastEightSeeds[i-9]);
                Assert.assertEquals("Seed of iteration " + i + " should be: " + currentTest.get("Z" + i), BigInt.compare(lastEightSeeds[i-9], comparison), 0);
            }


            String byteValueComparison = getByteValue(currentTest.get("B"));
            Assert.assertEquals("Generated bitString does not match comparison: " + byteValueComparison, byteValue, byteValueComparison);
        }
    }

    private String getByteValue(String rawByteValue) {
        String byteValue = rawByteValue;

        if (rawByteValue.charAt(1) == '0' && rawByteValue.charAt(2) == 'b')
            byteValue = rawByteValue.substring(3);

        // fill missing bits to bitsize of 16
        for (int i = byteValue.length(); i < 16; i++)
            byteValue = "0" + byteValue;
        return byteValue;

    }

    private static <T> T[] concatArrays(T[] first, T[] second) {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }
}

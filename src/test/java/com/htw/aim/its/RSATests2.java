package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class RSATests2 {

    final private String PATH = "/rsa";
    final private String FILENAME = "/RSA-Tests-2.txt";
    final private String NUMBER_INDEX = "abcdefghi";

    @Test
    public void testRSAEncrypt() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt p = BigInt.toBigIntFromHex(currentTest.get("p"));
            BigInt q = BigInt.toBigIntFromHex(currentTest.get("q"));
            BigInt e = BigInt.toBigIntFromHex(currentTest.get("e"));

            BigInt dComparison = BigInt.toBigIntFromHex(currentTest.get("d"));

            RSAKeys keys = RSAKeys.generateRSAKeys(e, 0, p, q);

            Assert.assertEquals("private key 'd' should be " + currentTest.get('D'), BigInt.compare(keys.getPrivateKey().getD(), dComparison), 0);

            BigInt plain;
            BigInt cypher;
            BigInt cypherCompare;

            plain = BigInt.toBigIntFromHex(currentTest.get("g"));
            cypherCompare = BigInt.toBigIntFromHex(currentTest.get("h"));
            cypher = RSA.encryptRSA(keys.getPublicKey(), plain);
            Assert.assertEquals("Encrypted value does not match comparison " + currentTest.get("H"), BigInt.compare(cypher, cypherCompare), 0);

            plain = BigInt.toBigIntFromHex(currentTest.get("i"));
            cypherCompare = BigInt.toBigIntFromHex(currentTest.get("j"));
            cypher = RSA.encryptRSA(keys.getPublicKey(), plain);
            Assert.assertEquals("Encrypted value does not match comparison " + currentTest.get("J"), BigInt.compare(cypher, cypherCompare), 0);

            plain = BigInt.toBigIntFromHex(currentTest.get("k"));
            cypherCompare = BigInt.toBigIntFromHex(currentTest.get("l"));
            cypher = RSA.encryptRSA(keys.getPublicKey(), plain);
            Assert.assertEquals("Encrypted value does not match comparison " + currentTest.get("L"), BigInt.compare(cypher, cypherCompare), 0);

        }
    }

    @Test
    public void testRSADecrypt() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt p = BigInt.toBigIntFromHex(currentTest.get("p"));
            BigInt q = BigInt.toBigIntFromHex(currentTest.get("q"));
            BigInt e = BigInt.toBigIntFromHex(currentTest.get("e"));

            BigInt dComparison = BigInt.toBigIntFromHex(currentTest.get("d"));

            RSAKeys keys = RSAKeys.generateRSAKeys(e, 0, p, q);

            Assert.assertEquals("private key 'd' should be " + currentTest.get('D'), BigInt.compare(keys.getPrivateKey().getD(), dComparison), 0);

            BigInt plain;
            BigInt cypher;
            BigInt plainCompare;

            cypher = BigInt.toBigIntFromHex(currentTest.get("h"));
            plainCompare = BigInt.toBigIntFromHex(currentTest.get("g"));
            plain = RSA.decryptRSA(keys.getPrivateKey(), cypher);
            Assert.assertEquals("Decrypted value does not match comparison " + currentTest.get("G"), BigInt.compare(plain, plainCompare), 0);

            cypher = BigInt.toBigIntFromHex(currentTest.get("j"));
            plainCompare = BigInt.toBigIntFromHex(currentTest.get("i"));
            plain = RSA.decryptRSA(keys.getPrivateKey(), cypher);
            Assert.assertEquals("Decrypted value does not match comparison " + currentTest.get("I"), BigInt.compare(plain, plainCompare), 0);

            cypher = BigInt.toBigIntFromHex(currentTest.get("l"));
            plainCompare = BigInt.toBigIntFromHex(currentTest.get("k"));
            plain = RSA.decryptRSA(keys.getPrivateKey(), cypher);
            Assert.assertEquals("Decrypted value does not match comparison " + currentTest.get("K"), BigInt.compare(plain, plainCompare), 0);

        }
    }
}

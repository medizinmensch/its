package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class PrimeNumberTests {

    final private String PATH = "/primzahlen";
    final private String FILENAME = "/primeNumber-Tests.txt";
    final private String NUMBER_INDEX = "abcdefghi";

    @Test
    public void testPrimeNumbersEuler() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));

            for (int i = 1; i < 13; i++) {
                String index = "a" + i;
                BigInt base = BigInt.toBigIntFromHex(currentTest.get(index));
                BigInt prim = BigInt.toBigIntFromHex(currentTest.get("p"));
                boolean isPrim = PrimeNumber.isPrimeEuler(prim, base);
                boolean shouldPrime = Boolean.parseBoolean(currentTest.get("e"));

                // result.debugString();
                Assert.assertEquals("Euler test should expect number '" + prim + "' to be prim.", shouldPrime, isPrim);
            }
        }
    }

    @Test
    public void testPrimeNumbersFermat() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));

            for (int i = 1; i < 13; i++) {
                String index = "a" + i;
                BigInt base = BigInt.toBigIntFromHex(currentTest.get(index));
                BigInt prim = BigInt.toBigIntFromHex(currentTest.get("p"));
                if (BigInt.equals(prim, new BigInt(2))) // skip number 2, no extra checking of 0, 1 & 2 or even numbers was wanted so fermat fails for number 2
                    continue;
                boolean isPrim = PrimeNumber.isPrimeFermat(prim, base);
                boolean shouldPrime = Boolean.parseBoolean(currentTest.get("f"));

                // result.debugString();
                Assert.assertEquals("Fermat test should expect number '" + prim + "' to be prim.", shouldPrime, isPrim);
            }
        }
    }

    @Test
    public void testPrimeNumbersMR() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));

            for (int i = 1; i < 13; i++) {
                String index = "a" + i;
                BigInt base = BigInt.toBigIntFromHex(currentTest.get(index));
                BigInt prim = BigInt.toBigIntFromHex(currentTest.get("p"));
                boolean isPrim = PrimeNumber.isPrimeMR(prim, base);
                boolean shouldPrime = Boolean.parseBoolean(currentTest.get("f"));

                // result.debugString();
                Assert.assertEquals("Miller Rabin test should expect number '" + prim + "' to be prim.", shouldPrime, isPrim);
            }
        }
    }
}

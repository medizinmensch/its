package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static com.htw.aim.its.ReportGenerator.*;


public class ConvertTests {

    final private String PATH = "/bigint";
    final private String FILENAME = "/Convert-Hex-Tests.txt";

    @Test
    public void testCompareDifferentBases() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);

        writeHeader("Compare Different Bases", 2);
        startCodeSection();
        for (HashMap<String, String> currentTest : testList) {
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt d = new BigInt(size, currentTest.get("d"));
            BigInt h = BigInt.toBigIntFromHex(currentTest.get("h"));
            BigInt o = BigInt.toBigIntFromOct(currentTest.get("o"));

            // if 0, numbers are same
            System.out.print(String.format("%s: Dec[%s] == Hex[%s]  |  ", currentTest.get("t"),d.toDecimalString(), h.toDecimalString()));
            Assert.assertEquals("Decimal and Hex number should be same.", BigInt.compare(d, h), 0);

            System.out.print(String.format("Hex[%s] == Dec[%s]\n",h.toDecimalString(), o.toDecimalString()));
            Assert.assertEquals("Hex and Oct number should be same", BigInt.compare(h, o), 0);
        }
        printFinalSection();
    }
}

package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class EulerPseudoTests {

    final private String PATH = "/primzahlen";
    final private String FILENAME = "/EulerPseudoPrime-Tests.txt";
    final private String NUMBER_INDEX = "abcdefghi";

    @Test
    public void testEulerPseudoPrime() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "', given number type: '" + currentTest.get("c") + "'.");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt base = BigInt.toBigIntFromHex(currentTest.get("a"));
            BigInt prim = BigInt.toBigIntFromHex(currentTest.get("p"));
            boolean isPrim = PrimeNumber.isPrimeEuler(prim, base);

            // result.debugString();
            Assert.assertTrue("Euler test should expect number '" + prim + "' to be prim.", isPrim);
        }
    }
}

package com.htw.aim.its;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

public class ShaTests {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    final private String FILENAME = "/sha256-tests.txt";

    @Test
    public void testSha256() throws Exception {
        ArrayList<Testcase> testcases = ShaTestFileReader.getTestsFromFileAsListAsObject(FILENAME);

        for (Testcase currentTest : testcases) {
            System.out.printf("%s%n", currentTest.values.get("t"));

            byte[] message = hexStringToByteArray(currentTest.values.get("p"));

            Sha256 hashGen = new Sha256();
            hashGen.sha256(message);

            Assert.assertEquals("Comparing l (message length in bits)", currentTest.sha.values.get("l"), String.format("+%d", hashGen.l));
            System.out.printf("comparing message length in bits  (l): %s%n", currentTest.sha.values.get("l").equals(String.format("+%d", hashGen.l)) ? "success" : "error");

            Assert.assertEquals("Comparing k (number of padded bits)", currentTest.sha.values.get("k"), String.format("+%d", hashGen.k));
            System.out.printf("comparing (number of padded bits) (k): %s%n", currentTest.sha.values.get("k").equals(String.format("+%d", hashGen.k)) ? "success" : "error");


            for (int blockIndex = 0; blockIndex < hashGen.debugW.size(); blockIndex++) {
                System.out.printf("Handling Block %d/%d:%n", blockIndex + 1, hashGen.debugW.size());
                int[] W_calculated_tmp = new int[16];
                System.arraycopy(hashGen.debugW.get(blockIndex), 0, W_calculated_tmp, 0, 16);
                String W_calculated = intArrToHexString(W_calculated_tmp, "+0x");
                String W_testFile = currentTest.sha.blocks.get(blockIndex).get("W");

                Assert.assertEquals("comparing scheduled message (W)", W_testFile, W_calculated);
                System.out.printf("comparing scheduled message       (W): %s%n", W_testFile.equals(W_calculated) ? "success" : "error");

                int[] H_calculated_tmp = new int[8];
                System.arraycopy(hashGen.debugH.get(blockIndex), 0, H_calculated_tmp, 0, 8);
                String H_calculated = intArrToHexString(H_calculated_tmp, "+0x");
                String H_testFile = currentTest.sha.blocks.get(blockIndex).get("H");

                Assert.assertEquals("comparing starting hash for block (H)", H_testFile, H_calculated);
                System.out.printf("comparing starting hash for block (H): %s%n", H_testFile.equals(H_calculated) ? "success" : "error");

                for (int R_index = 0; R_index < 64; R_index++) {
                    String R_testFile = currentTest.sha.blocks.get(blockIndex).get(String.format("R[%02d]", R_index));
                    String R_calculated = hashGen.debugR.get(blockIndex)[R_index];
                    Assert.assertEquals(String.format("comparing R[%02d] (intermediate result)", R_index), R_testFile, R_calculated);
//                    System.out.printf("comparing R[%02d]: %s%n", R_index, rIsEqual ? "success" : "error");
                }
                System.out.println("comparing intermediary rounds   (R's): success");

                Assert.assertEquals("comparing h (sha256 message digest of p)", currentTest.values.get("h"), intArrToHexString(hashGen.h, ""));
                System.out.printf("comparing final hash              (h): %s%n", currentTest.values.get("h").equals(String.format("%s", intArrToHexString(hashGen.h, ""))) ? "success" : "error");
            }
            System.out.println("");
        }
    }

    @Test
    public void testShaSha256() throws Exception {
        ArrayList<Testcase> testcases = ShaTestFileReader.getTestsFromFileAsListAsObject(FILENAME);

        for (Testcase currentTest : testcases) {
            System.out.printf("%s%n", currentTest.values.get("t"));

            byte[] message = hexStringToByteArray(currentTest.values.get("h"));

            Sha256 hashGen = new Sha256();
            hashGen.sha256(message);

            Assert.assertEquals("Comparing l (message length in bits)", currentTest.shasha.values.get("l"), String.format("+%d", hashGen.l));
            System.out.printf("comparing message length in bits  (l): %s%n", currentTest.shasha.values.get("l").equals(String.format("+%d", hashGen.l)) ? "success" : "error");

            Assert.assertEquals("Comparing k (number of padded bits)", currentTest.shasha.values.get("k"), String.format("+%d", hashGen.k));
            System.out.printf("comparing (number of padded bits) (k): %s%n", currentTest.shasha.values.get("k").equals(String.format("+%d", hashGen.k)) ? "success" : "error");


            for (int blockIndex = 0; blockIndex < hashGen.debugW.size(); blockIndex++) {
                System.out.printf("Handling Block %d/%d:%n", blockIndex + 1, hashGen.debugW.size());
                int[] W_calculated_tmp = new int[16];
                System.arraycopy(hashGen.debugW.get(blockIndex), 0, W_calculated_tmp, 0, 16);
                String W_calculated = intArrToHexString(W_calculated_tmp, "+0x");
                String W_testFile = currentTest.shasha.blocks.get(blockIndex).get("W");

                Assert.assertEquals("comparing scheduled message (W)", W_testFile, W_calculated);
                System.out.printf("comparing scheduled message       (W): %s%n", W_testFile.equals(W_calculated) ? "success" : "error");

                int[] H_calculated_tmp = new int[8];
                System.arraycopy(hashGen.debugH.get(blockIndex), 0, H_calculated_tmp, 0, 8);
                String H_calculated = intArrToHexString(H_calculated_tmp, "+0x");
                String H_testFile = currentTest.shasha.blocks.get(blockIndex).get("H");

                Assert.assertEquals("comparing starting hash for block (H)", H_testFile, H_calculated);
                System.out.printf("comparing starting hash for block (H): %s%n", H_testFile.equals(H_calculated) ? "success" : "error");

                for (int R_index = 0; R_index < 64; R_index++) {
                    String R_testFile = currentTest.shasha.blocks.get(blockIndex).get(String.format("R[%02d]", R_index));
                    String R_calculated = hashGen.debugR.get(blockIndex)[R_index];
                    Assert.assertEquals(String.format("comparing R[%02d] (intermediate result)", R_index), R_testFile, R_calculated);
//                    System.out.printf("comparing R[%02d]: %s%n", R_index, rIsEqual ? "success" : "error");
                }
                System.out.println("comparing intermediary rounds   (R's): success");

                Assert.assertEquals("comparing d (shasha256 msg digest of h)", currentTest.values.get("d"), intArrToHexString(hashGen.h, "+0x"));
                System.out.printf("comparing final hash            (h/d): %s%n", currentTest.values.get("d").equals(String.format("%s", intArrToHexString(hashGen.h, "+0x"))) ? "success" : "error");
            }
            System.out.println("");
        }
    }


    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static String intArrToHexString(int[] arr, String prepend) {
        StringBuilder text = new StringBuilder(prepend);
        for (int b : arr) {
            text.append(String.format("%08x", b));
        }
        return text.toString();
    }
}

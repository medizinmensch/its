package com.htw.aim.its;

import com.htw.aim.its.domain.EgcdTuple;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class EgcdTests {

    final private String PATH = "/rsa";
    final private String FILENAME = "/egcd-Tests.txt";
    final private String NUMBER_INDEX = "abcdefghi";

    @Test
    public void testEgcd() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt a = BigInt.toBigIntFromHex(currentTest.get("a"));
            BigInt b = BigInt.toBigIntFromHex(currentTest.get("b"));
            BigInt comparison = BigInt.toBigIntFromHex(currentTest.get("g"));
            BigInt uComparison = BigInt.toBigIntFromHex(currentTest.get("u"));
            BigInt vComparison = BigInt.toBigIntFromHex(currentTest.get("v"));
            EgcdTuple egcd = BigInt.egcdMod(a, b); // egcdBin was not providing the right u & v values
            System.out.println("\tGCD: " + egcd.getGcd() + "   (should be: " + comparison + ")");
            System.out.println("\tU: " + egcd.getU() + " * " + a + "   (should be: " + uComparison + ")");
            System.out.println("\tV: " + egcd.getV() + " * " + b + "   (should be: " + vComparison + ")");
            //System.out.println("\tU: " + egcd.getU() + "   (should be: " + uComparison + ")");
            //System.out.println("\tV: " + egcd.getV() + "   (should be: " + vComparison + ")");

            Assert.assertTrue("egcd of a & b (" + egcd.getGcd() + ") does not equal comparison: " + comparison, BigInt.equals(comparison, egcd.getGcd()));
            Assert.assertTrue("u of b (" + egcd.getU() + ") does not equal comparison: " + uComparison, BigInt.equals(uComparison, egcd.getU()));
            Assert.assertTrue("v of b (" + egcd.getV() + ") does not equal comparison: " + vComparison, BigInt.equals(vComparison, egcd.getV()));
        }
    }
}

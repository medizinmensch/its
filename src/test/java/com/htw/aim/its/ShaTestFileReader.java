package com.htw.aim.its;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class ShaTestFileReader {
    public static ArrayList<Testcase> getTestsFromFileAsListAsObject(String testFilePath) throws Exception {
        ReportGenerator.writeHeader(testFilePath.substring(1, testFilePath.length() - 4), 1);
        ArrayList<Testcase> testcases = new ArrayList<>();

        // read file
        Class clazz = ShaTestFileReader.class;
        InputStream inputStream = clazz.getResourceAsStream(testFilePath);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            Testcase currentTestcase = new Testcase();
            Digest workingDigest = new Digest();
            HashMap<String, String> workingBlock = new HashMap<String, String>();

            String line;
            while ((line = br.readLine()) != null) {
                line = line.replaceAll(" ", "");

                if (line.charAt(0) != '#') {
                    String[] lineArray = line.split("=");
                    char fc = line.charAt(0);

                    if (fc == 't' || fc == 'p' || fc == 'h' || fc == 'd')
                        currentTestcase.values.put(lineArray[0], lineArray[1]);
                    if (fc == 'l' || fc == 'k')
                        workingDigest.values.put(lineArray[0], lineArray[1]);
                    else if (fc == 'H' || fc == 'W' || fc == 'R'){
                        workingBlock.put(lineArray[0], lineArray[1]);
                        if (line.contains("R[63")) {
                            workingDigest.blocks.add(workingBlock);
                            workingBlock = new HashMap<String, String>();
                        }
                    }
                } else if (line.contains("sha256(sha256(...))")) {
                    currentTestcase.sha = (Digest) workingDigest.clone();
                    workingDigest = new Digest();
                } else if (line.contains("--------------------------")) {
                    currentTestcase.shasha = (Digest) workingDigest.clone();
                    testcases.add((Testcase) currentTestcase.clone());
                    currentTestcase = new Testcase();
                    workingDigest = new Digest();
                }
            }
        }
        return testcases;
    }

    public static ArrayList<HashMap<String, String>> getTestsFromFileAsList(String testFilePath) throws Exception {

        ReportGenerator.writeHeader(testFilePath.substring(1, testFilePath.length() - 4), 1);
        ArrayList<HashMap<String, String>> testList = new ArrayList<>();

        // read file
        Class clazz = ShaTestFileReader.class;
        InputStream inputStream = clazz.getResourceAsStream(testFilePath);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;

            // line by line
            HashMap<String, String> currentTestCase = new HashMap<>();
            while ((line = br.readLine()) != null) {
                line = line.replaceAll(" ", "");

                if (currentTestCase.containsKey("d")) { // last variable to save
                    testList.add(currentTestCase);
                    currentTestCase = new HashMap<>();
                }
                if (line.contains("=")) {
                    String[] lineArray = line.split("=");
//                    if(currentTestCase.containsKey(lineArray[0]))
                    if (currentTestCase.containsKey(lineArray[0])) // all that show up twice get the "-r" for reverse (sha(sha()))
                        lineArray[0] += "-r";
                    currentTestCase.put(lineArray[0], lineArray[1]);
                }
                if (line.contains("beginofnextblock")) {

                }
            }
        }
        return testList;
    }
}

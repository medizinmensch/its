package com.htw.aim.its;

import com.htw.aim.its.domain.BBS;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class BBS_findPAndQTests {

    final private String PATH = "/bbs";
    final private String FILENAME = "/Finden von p und q.txt";

    @Test
    public void findPAndQ() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt startValue = BigInt.toBigIntFromHex(currentTest.get("a"));
            BBS currentBBS = new BBS(new BigInt("5"), startValue);

            BigInt pComparison = BigInt.toBigIntFromHex(currentTest.get("p"));
            BigInt qComparison = BigInt.toBigIntFromHex(currentTest.get("q"));
            BigInt nComparison = BigInt.toBigIntFromHex(currentTest.get("n"));
            Assert.assertEquals("Found p for BBS should be: " + currentTest.get("P"), BigInt.compare(currentBBS.getP(), pComparison), 0);
            Assert.assertEquals("Found q for BBS should be: " + currentTest.get("Q"), BigInt.compare(currentBBS.getQ(), qComparison), 0);
            Assert.assertEquals("Found n for BBS should be: " + currentTest.get("N"), BigInt.compare(currentBBS.getN(), nComparison), 0);

        }
    }
}

package com.htw.aim.its;

public class ReportGenerator {
    public static void writeHeader(String heading, int level) {
        for (int i = 0; i < level; i++) {
            System.out.print("#");
        }
        System.out.print(String.format(" %s\n\n", heading));
    }

    public static void startCodeSection() {
        System.out.print("```\n");
    }

    public static void printFinalSection(){
        System.out.print("```\n");
    }

    public static void printTestResult(String operator, String currentTest, String a, String b, String result, String comparison) {
        System.out.print(String.format("%s: %s %s %s = %s (should be: %s)\n", currentTest, a, operator, b, result, comparison));
    }
}

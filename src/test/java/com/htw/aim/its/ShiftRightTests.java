package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static com.htw.aim.its.ReportGenerator.*;


public class ShiftRightTests {

    final private String PATH = "/bigint";
    final private String FILENAME = "/Shift-Right-Tests.txt";
    final private String NUMBER_INDEX = "abcdefgh";
    final private int BITS_PER_CELL = BigIntConfig.CELL_SIZE;

    @Test
    public void testShiftRight() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);

        writeHeader("Shift right", 2);
        startCodeSection();
        for (HashMap<String, String> currentTest : testList) {
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt a = BigInt.toBigIntFromHexWithSize(size / BITS_PER_CELL, currentTest.get("a"));
            BigInt comparison;

            for (int i = 1; i < 8; i++) {
                String currentChar = NUMBER_INDEX.charAt(i) + "";
                comparison = BigInt.toBigIntFromHexWithSize(size / BITS_PER_CELL, currentTest.get(currentChar));

                System.out.print(String.format("%s >> %d = ", currentTest.get("a"), i));
                BigInt.shiftRight(a);
                System.out.print(String.format("%s (should be %s)\n", currentTest.get("a"), comparison.toDecimalString()));

                Assert.assertEquals("Shifted a is not equal to current iteration '" + i + "'.\n value: " + a + "\ncomparison: " + currentTest.get(currentChar), BigInt.compare(a, comparison), 0);
            }
        }
        printFinalSection();
    }
}

package com.htw.aim.its;

import com.htw.aim.its.BigInt;
import com.htw.aim.its.TestFileReader;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static com.htw.aim.its.ReportGenerator.*;


public class CompareTests {

    final private String PATH = "/bigint";
    final private String FILENAME = "/Compare-Tests.txt";

    @Test
    public void testCompareAAndB() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);

        writeHeader("A and B", 2);
        startCodeSection();
        for (HashMap<String, String> currentTest : testList) {
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt a = new BigInt(size, currentTest.get("A"));
            BigInt b = new BigInt(size, currentTest.get("B"));
            int result = BigInt.compare(a, b);
            int c = Integer.parseInt(currentTest.get("c"));

            printTestResult("compared to", currentTest.get("t"), a.toDecimalString(), b.toDecimalString(), Integer.toString(result), Integer.toString(c));
            Assert.assertEquals("Comparison was wrong.", result, c);
        }
        printFinalSection();
    }

    @Test
    public void testCompareBAndA() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);

        writeHeader("B and A", 2);
        startCodeSection();
        for (HashMap<String, String> currentTest : testList) {
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt a = new BigInt(size, currentTest.get("A"));
            BigInt b = new BigInt(size, currentTest.get("B"));
            int result = BigInt.compare(b, a);
            int c = Integer.parseInt(currentTest.get("d"));

            printTestResult("compared to", currentTest.get("t"), a.toDecimalString(), b.toDecimalString(), Integer.toString(result), Integer.toString(c));
            Assert.assertEquals("Comparison was wrong.", result, c);
        }
        printFinalSection();
    }
}

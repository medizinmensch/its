package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static com.htw.aim.its.ReportGenerator.*;


public class Div10Tests {

    final private String PATH = "/bigint";
    final private String FILENAME = "/Div10-Tests.txt";
    final private String NUMBER_INDEX = "abcdefgh";

    @Test
    public void testDiv10() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);

        writeHeader("Divide by 10", 2);
        startCodeSection();
        for (HashMap<String, String> currentTest : testList) {
            BigInt a = BigInt.toBigIntFromHex(currentTest.get("a"));
            BigInt comparison;

            for (int i = 1; i < 8; i++) {
                String currentChar = NUMBER_INDEX.charAt(i) + "";
                comparison = BigInt.toBigIntFromHex(currentTest.get(currentChar));

                System.out.print(String.format("%s-%d: %s / 10 = ", currentTest.get("t"), i, a.toDecimalString()));
                BigInt.divmod10(a);
                System.out.print(String.format("%s (should be %s)\n", a.toDecimalString(), comparison.toDecimalString()));

                Assert.assertEquals("Divided (by 10) number is not equal to current iteration '" + i + "'", BigInt.compare(a, comparison), 0);
            }
        }
        printFinalSection();
    }
}

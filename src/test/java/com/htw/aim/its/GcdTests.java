package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class GcdTests {

    final private String PATH = "/primzahlen";
    final private String FILENAME = "/gcd-Tests.txt";
    final private String NUMBER_INDEX = "abcdefghi";

    @Test
    public void testGcd() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt a = BigInt.toBigIntFromHex(currentTest.get("a"));
            BigInt b = BigInt.toBigIntFromHex(currentTest.get("b"));
            BigInt comparison = BigInt.toBigIntFromHex(currentTest.get("g"));
            BigInt gcd = BigInt.gcdBin(a, b);

            // result.debugString();
            Assert.assertEquals("gcd of a & b does not equal comparison: " + currentTest.get("g"), BigInt.compare(gcd, comparison), 0);
        }
    }
}

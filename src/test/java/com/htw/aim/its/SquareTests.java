package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class SquareTests {

    final private String PATH = "/primzahlen";
    final private String FILENAME = "/Square-Tests.txt";
    final private String NUMBER_INDEX = "abcdefghi";

    @Test
    public void testSquare() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt a = new BigInt(currentTest.get("A"));
            BigInt comparison = new BigInt(currentTest.get("B"));
            BigInt result = BigInt.square(a);

            //result.debugString();
            Assert.assertEquals("a*a does not equal comparison: " + currentTest.get("B"), BigInt.compare(result, comparison), 0);
        }
    }
}

package com.htw.aim.its;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.HashMap;

import static com.htw.aim.its.ReportGenerator.*;


public class ShiftLeftTests {

    final private String PATH = "/bigint";
    final private String FILENAME = "/Shift-Left-Tests.txt";
    final private String NUMBER_INDEX = "abcdefgh";
    final private int BITS_PER_CELL = BigIntConfig.CELL_SIZE;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testShiftLeft() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);

        writeHeader("Shift Left", 2);
        startCodeSection();
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt comparison;

            for (int i = 1; i < 8; i++) {
                BigInt a = BigInt.toBigIntFromHexWithSize(size / BITS_PER_CELL, currentTest.get("a"));
                String currentChar = NUMBER_INDEX.charAt(i) + "";
                comparison = BigInt.toBigIntFromHexWithSize(size / BITS_PER_CELL, currentTest.get(currentChar));

                int testNumber = Integer.parseInt(testName.split("-")[1]);

                // condition to find all cases where shifting left would reach maximum size
                if (testNumber > 7 && testNumber < 14 || testNumber == 7 && i >= 2 || testNumber == 14 && i >= 2) {
                    int shift = i;
                    Assertions.assertThrows(Exception.class, () -> BigInt.shiftLeftWithResize(a, shift, false));
                    exception.expect(Exception.class);
                }
                else {
                    System.out.print(String.format("%s << %d = ", currentTest.get("a"), i));
                    BigInt.shiftLeft(a, i);
                    System.out.print(String.format("%s (should be %s)\n", currentTest.get("a"), comparison.toDecimalString()));

                    Assert.assertEquals("Shifted a is not equal to current iteration '" + i + "'", BigInt.compare(a, comparison), 0);
                }
            }
        }
        printFinalSection();
    }
}

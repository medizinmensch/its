package com.htw.aim.its;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class NoPrimeNumberTests {

    final private String PATH = "/primzahlen";
    final private String FILENAME = "/NoPrimeNumber-Tests.txt";
    final private String NUMBER_INDEX = "abcdefghi";

    @Test
    public void testPrimeNumbersEuler() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));

            for (int i = 1; i < 13; i++) {
                BigInt base = BigInt.toBigIntFromHex(currentTest.get("a"));
                BigInt prim = BigInt.toBigIntFromHex(currentTest.get("p"));
                boolean isPrim = PrimeNumber.isPrimeEulerNoGcd(prim, base);
                boolean shouldPrime = Boolean.parseBoolean(currentTest.get("e"));

                // result.debugString();
                Assert.assertEquals("Euler test should expect number '" + prim + "' to not be prim.", shouldPrime, isPrim);
            }
        }
    }

    @Test
    public void testPrimeNumbersFermat() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));

            // some of the given test numbers are only "failing" if we use condition1 or cond2?!?!
            // the case when prime=1 (test cases 1-12) is getting 0 when using cond1 sure, but for condition2 the test works fine?
            // condition1: a^(n-1) = 1 (mod n) with a>0, gcd(a,n) = 1 and n is prim
            // condition2: a^n = a (mod n) with a>0 and n is prim
            ArrayList<Integer> cond2List = new ArrayList<>(Arrays.asList(35, 36, 47, 48));

            for (int i = 1; i < 13; i++) {
                boolean isPrim;
                BigInt base = BigInt.toBigIntFromHex(currentTest.get("a"));
                BigInt prim = BigInt.toBigIntFromHex(currentTest.get("p"));
                boolean shouldPrime = Boolean.parseBoolean(currentTest.get("f"));
                int testNumber = Integer.parseInt(testName.split("-")[1]);
                if (testNumber < 13 || cond2List.contains(testNumber))
                    isPrim = PrimeNumber.isPrimeFermat(prim, base);
                else
                    isPrim = PrimeNumber.isPrimeFermatNoGcd(prim, base);


                // result.debugString();
                Assert.assertEquals("Fermat test should expect number '" + prim + "' to not be prim.", shouldPrime, isPrim);
            }
        }
    }


}

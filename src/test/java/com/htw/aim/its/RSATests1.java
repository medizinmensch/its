package com.htw.aim.its;

import com.htw.aim.its.domain.EgcdTuple;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;


public class RSATests1 {

    final private String PATH = "/rsa";
    final private String FILENAME = "/RSA-Tests-1.txt";
    final private String NUMBER_INDEX = "abcdefghi";

    @Test
    public void testRSAKeyGeneration() throws Exception {
        ArrayList<HashMap<String, String>> testList = TestFileReader.getTestsFromFileAsList(PATH + FILENAME);
        for (HashMap<String, String> currentTest : testList) {
            String testName = currentTest.get("t");
            System.out.println("Test: '" + testName + "'");
            int size = Integer.parseInt(currentTest.get("s"));
            BigInt p = BigInt.toBigIntFromHex(currentTest.get("p"));
            BigInt q = BigInt.toBigIntFromHex(currentTest.get("q"));
            BigInt e = BigInt.toBigIntFromHex(currentTest.get("e"));

            BigInt phiComparison = BigInt.toBigIntFromHex(currentTest.get("f"));
            BigInt dComparison = BigInt.toBigIntFromHex(currentTest.get("d"));

            RSAKeys keys = RSAKeys.generateRSAKeys(e, 0, p, q);

            Assert.assertEquals("private key 'd' should be " + currentTest.get('D'), BigInt.compare(keys.getPrivateKey().getD(), dComparison), 0);
            Assert.assertEquals("phi should be " + currentTest.get('F'), BigInt.compare(keys.getPhi(), phiComparison), 0);
        }
    }
}

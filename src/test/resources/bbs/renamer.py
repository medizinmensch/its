

# read line by line

# skip everything

# loop each test
# reset by "#--------------------------"
# count Z up



with open("BBS-Tests.txt", "r") as a_file:
    z_counter = 1
    for line in a_file:
        stripped_line = line.strip()

        if "# " in stripped_line:
            print(stripped_line)
        elif "#--------------------------" in stripped_line:
            print(stripped_line)
            z_counter = 1
        elif "z=" in stripped_line:
            replacer = f"z{z_counter}"
            print(stripped_line.replace('z', replacer))
        elif "Z=" in stripped_line:
            replacer = f"Z{z_counter}"
            print(stripped_line.replace('Z', replacer))
            z_counter += 1
        else:
            print(stripped_line)

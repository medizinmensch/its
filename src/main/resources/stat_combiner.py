

filepath = "exampleStats.txt"
mega_dict = {}

with open(filepath, "r") as a_file:
    for line in a_file:
        if line == "\n":
            continue

        number = line.split(',')[0]
        amount = int(line.split(',')[1].replace('\n', ''))
        if number not in mega_dict:
            mega_dict[number] = amount
        else:
            mega_dict[number] = mega_dict[number] + amount


#print(mega_dict)
min = 1000
max = 0
avg = 0
amount_of_numbers = 0
iterations = 0
with open("stats_final.txt", "w") as output_file:
	for item in mega_dict:
		if mega_dict[item] > max:
			max = mega_dict[item]
		if mega_dict[item] < min:
			min = mega_dict[item]
		iterations += 1
		amount_of_numbers += int(mega_dict[item])
		avg += int(mega_dict[item]) * int(item)
		output_file.write(f"{item}: {mega_dict[item]}\n")

print(f"amount of numbers: {amount_of_numbers}")
print(f"iterations: {iterations}")
print(f"avg of number occurence: {amount_of_numbers/iterations}")
print(f"max: {max}")
print(f"min: {min}")
print(f"avg value: {avg/amount_of_numbers}")

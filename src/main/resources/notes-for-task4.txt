# Bezüglich "BBS_findPTests" & "BBS_findPAndQTests"

Das Finden der Primzahlen dauert hier wirklich sehr lange (~8 Stunden).

Außerdem schlagen manchmal die BBS_findPTests beim ersten Ausführen fehl. Beim erneuten Starten laufen sie dann aber durch. Wir wissen nicht genau warum es beim ersten Mal nicht funktioniert.
package com.htw.aim.its.domain;

import com.htw.aim.its.BigInt;
import com.htw.aim.its.PrimeNumber;


public class BBS {

    private final BigInt ONE = new BigInt("1");
    private final BigInt TWO = new BigInt("2");
    private final BigInt THREE = new BigInt("3");
    private final BigInt FOUR = new BigInt("4");

    private BigInt seed;
    private BigInt p;
    private BigInt q;
    private BigInt n;
    private BigInt[] lastSeeds;
    private int size;

    // used for setting bitsize of n
    public BBS(BigInt seed, int size) throws Exception {
        // if seed < 2
        if (BigInt.compare(seed, TWO) == -1)
            throw new Exception("Seed can't be 0 or 1.");

        System.out.println("INFO - Setting up parameters for Blum-Bum-Shub random number generation.");

        BigInt p;
        BigInt q;
        BigInt n = new BigInt("0");

        do {
            do {
                System.out.println("INFO - Finding p with p mod 4 = 3");
                p = PrimeNumber.findPrime(size / 2 + 5);
            } while (BigInt.equals(BigInt.divmod(p, FOUR, new BigInt("0")), THREE));
            System.out.println("INFO - Found p");

            BigInt pPlus25Percent = new BigInt("0");
            BigInt quarterP = new BigInt("0");
            BigInt.divmod(p, FOUR, quarterP);
            BigInt.add(p, quarterP, pPlus25Percent);

            do {
                System.out.println("INFO - Finding q with q mod 4 = 3");
                q = PrimeNumber.findPrime(size / 2 + 5);
            } while (!BigInt.equals(BigInt.divmod(q, FOUR, new BigInt("0")), THREE) && !BigInt.equals(p, q));
            System.out.println("INFO - Found q");

            BigInt.mul(p, q, n);
        } while (BigInt.gcdBin(seed, n) == ONE);

        //todo: check cases if n cant be bigger then seed
        if (BigInt.compare(seed, n) == 1)
            System.out.println("WARN: seed is bigger then n.");

        this.seed = seed;
        this.p = p;
        this.q = q;
        this.size = n.bitLength();
        this.n = n;
        System.out.println("INFO - Finished BBS setup, size of n: " + this.size);
    }

    // used for input with minimum startValue for p
    public BBS(BigInt seed, BigInt startValue) throws Exception {
        // if seed < 2
        if (BigInt.compare(seed, TWO) == -1)
            throw new Exception("Seed can't be 0 or 1.");

        System.out.println("INFO - Setting up parameters for Blum-Bum-Shub random number generation.");

        BigInt p;
        BigInt q;
        BigInt n = new BigInt("0");

        do {
            do {
                System.out.println("INFO - Finding p with p mod 4 = 3");
                p = PrimeNumber.findPrimeLinearlyWithStartValue(startValue);
                startValue = p;
            } while (!BigInt.equals(BigInt.divmod(p, FOUR, new BigInt("0")), THREE));
            System.out.println("INFO - Found p: " + p);

            BigInt pPlus25Percent = new BigInt("0");
            BigInt quarterP = new BigInt("0");
            BigInt.divmod(p, FOUR, quarterP);
            BigInt.add(p, quarterP, pPlus25Percent);

            do {
                System.out.println("INFO - Finding q with q mod 4 = 3");
                q = PrimeNumber.findPrimeLinearlyWithStartValue(pPlus25Percent);
                pPlus25Percent = q;
            } while (!BigInt.equals(BigInt.divmod(q, FOUR, new BigInt("0")), THREE) && !BigInt.equals(p, q));
            System.out.println("INFO - Found q: " + q);

            BigInt.mul(p, q, n);
        } while (BigInt.gcdBin(seed, n) == ONE);

        //todo: check cases if n cant be bigger then seed
        if (BigInt.compare(seed, n) == 1)
            System.out.println("WARN: seed is bigger then n.");

        this.seed = seed;
        this.p = p;
        this.q = q;
        this.size = n.bitLength();
        this.n = n;
        System.out.println("INFO - Finished BBS setup, size of n: " + this.size);
    }

    // used when giving p & q
    public BBS(BigInt seed, BigInt p, BigInt q) throws Exception {
        // if seed < 2
        if (BigInt.compare(seed, TWO) == -1)
            throw new Exception("Seed can't be 0 or 1.");

        System.out.println("INFO - Setting up parameters for Blum-Bum-Shub random number generation.");

        BigInt n = new BigInt("0");
        BigInt.mul(p, q, n);
        if (BigInt.gcdBin(seed, n) == ONE)
            System.out.println("WARN - Given seed, p & q does not work for BBS, gcd(n & seed) == 1");

        if (BigInt.compare(seed, n) == 1)
            System.out.println("WARN: seed is bigger then n.");

        this.seed = seed;
        this.p = p;
        this.q = q;
        this.size = n.bitLength();
        this.n = n;
        System.out.println("INFO - Finished BBS setup, size of n: " + this.size);
    }

    public String getNextByte() throws Exception {
        // todo threading? - call process 8 times for 8 bit
        BigInt w;
        lastSeeds = new BigInt[8];

        StringBuilder r = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            w = BigInt.powerMod(seed, TWO, n);
            lastSeeds[i] = w;
            if (w.isOdd())
                r.append("1");
            else
                r.append("0");
            seed = (BigInt) w.clone();
        }
        // this conversion was not useful, so just string was returned
        //int tmp = Integer.parseInt(r.toString());
        //return (byte) tmp;
        return r.toString();
    }

    public static BigInt findPrimeForBBS(BigInt startValue) throws Exception {
        BigInt prime;
        BigInt THREE = new BigInt("3");
        BigInt FOUR = new BigInt("4");
        do {
            System.out.println("INFO - Finding prime with p mod 4 = 3");
            prime = PrimeNumber.findPrimeLinearlyWithStartValue(startValue);
            startValue = prime;
        } while (!BigInt.equals(BigInt.divmod(prime, FOUR, new BigInt("0")), THREE));
        return prime;
    }

    public BigInt getP() {
        return p;
    }

    public BigInt getQ() {
        return q;
    }

    public BigInt getN() {
        return n;
    }

    public int getSize() {
        return size;
    }

    public BigInt getSeed() {
        return seed;
    }

    public BigInt[] getLastSeeds() {
        return lastSeeds;
    }
}

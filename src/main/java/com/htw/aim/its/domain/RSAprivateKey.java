package com.htw.aim.its.domain;

import com.htw.aim.its.BigInt;

public class RSAprivateKey {
    private BigInt d;
    private BigInt n;

    private BigInt p;
    private BigInt q;

    public RSAprivateKey(BigInt d, BigInt n, BigInt p, BigInt q) {
        this.d = d;
        this.n = n;
        this.p = p;
        this.q = q;
    }

    public BigInt getD() {
        return this.d;
    }
    public BigInt getN() {
        return this.n;
    }
    public BigInt getP() {
        return this.p;
    }
    public BigInt getQ() {
        return this.q;
    }

    @Override
    public String toString() {
        return "private Key: {" + d + ":" + n + "}";
    }
}

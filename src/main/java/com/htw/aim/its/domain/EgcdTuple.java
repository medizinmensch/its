package com.htw.aim.its.domain;

import com.htw.aim.its.BigInt;

public class EgcdTuple {
    private BigInt gcd;
    private BigInt u;
    private BigInt v;

    public EgcdTuple(BigInt b, BigInt bu, BigInt bv) {
        this.gcd = b;
        this.u = bu;
        this.v = bv;
    }

    public BigInt getGcd() {
        return this.gcd;
    }

    public BigInt getU() {
        return this.u;
    }

    public BigInt getV() {
        return this.v;
    }
}

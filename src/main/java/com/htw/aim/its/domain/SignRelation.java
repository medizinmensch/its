package com.htw.aim.its.domain;

public enum SignRelation {POSITIVE_POSITIVE, POSITIVE_NEGATIVE, NEGATIVE_NEGATIVE, NEGATIVE_POSITIVE}

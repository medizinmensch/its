package com.htw.aim.its.domain;

import com.htw.aim.its.BigInt;

public class RSApublicKey {
    private BigInt e;
    private BigInt n;

    public RSApublicKey(BigInt e, BigInt n) {
        this.e = e;
        this.n = n;
    }

    public BigInt getE() {
        return this.e;
    }

    public BigInt getN() {
        return this.n;
    }

    @Override
    public String toString() {
        return "public Key: {" + e + ":" + n + "}";
    }
}

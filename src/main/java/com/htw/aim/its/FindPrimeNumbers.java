package com.htw.aim.its;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class FindPrimeNumbers {
    static Random random = new Random();

    public static BigInt findPrimeForGivenBases(int bitLength, BigInt[] bases) throws Exception {
        BigInt number = PrimeNumber.getRandomOdd(bitLength);

        while (!PrimeNumber.isPrime(number, bases)) {
            number = PrimeNumber.getRandomOdd(bitLength);
        }

        System.out.println("~~~ Found Prime ~~~ ");
        System.out.println(number);
        System.out.println("~~~             ~~~ ");
        return number;
    }

    static void comparePrimeNumberTests(String path) throws Exception {
        BigInt[] numbers = getNumbersFromFile(path);
        numbers = select100Numbers(numbers);
        System.out.println("Selected numbers for tests:");
        System.out.println(Arrays.toString(numbers));


        int amountOfPrims = numbers.length;
        int amountOfBases = 20;
        System.out.println("Comparing different tests to find prim numbers for " + amountOfPrims + " numbers.");
        System.out.println("Amount of additional bases: " + amountOfBases);

        // do it 100 times and calc average
        int iterations = 100;
        boolean[] divResults = new boolean[amountOfPrims];
        boolean[] fermatResults = new boolean[amountOfPrims];
        boolean[] eulerResults = new boolean[amountOfPrims];
        boolean[] mrResults = new boolean[amountOfPrims];
        double divPercentage = 0, fermatPercentage = 0, eulerPercentage = 0, mrPercentage = 0;

        for (int i = 0; i < iterations; i++) {
            BigInt[] bases = PrimeNumber.generateBasesWithGivenPrimes(amountOfBases, 5000, PrimeNumber.PRIM_BASES);

            // execute each test
            for (int j = 0; j < amountOfPrims; j++) {
                BigInt primToTest = numbers[j];
                //int bitLength = random.nextInt((60 - 3) + 1) + 3;
                //BigInt number = PrimeNumber.getRandomOdd(bitLength);

                divResults[j] = PrimeNumber.isPrimeDiv(primToTest, PrimeNumber.PRIMS_UNDER_100);
                fermatResults[j] = PrimeNumber.isPrimeFermat(primToTest, bases);
                eulerResults[j] = PrimeNumber.isPrimeEuler(primToTest, bases);
                mrResults[j] = PrimeNumber.isPrimeMR(primToTest, bases);
            }

            // calc percentages
            divPercentage += countBooleans(divResults);
            fermatPercentage += countBooleans(fermatResults);
            eulerPercentage += countBooleans(eulerResults);
            mrPercentage += countBooleans(mrResults);

            System.out.println("Iteration " + (i + 1) + "/" + (iterations) + ": Div --- Fermat --- Euler --- MR: ");
            System.out.println("\t\t\t   " + countBooleans(divResults) + " --- " + countBooleans(fermatResults) + "     --- " + countBooleans(eulerResults) + "   --- " + countBooleans(mrResults));
        }

        // calc final percentages
        System.out.println("Final calc: Div --- Fermat --- Euler --- MR: ");
        System.out.println("\t\t   " + divPercentage / iterations + " --- " + fermatPercentage / iterations + "     --- " + eulerPercentage / iterations + "   --- " + mrPercentage / iterations);
    }

    static void findPrimeNumbers(int amount, int maxBitSize) throws Exception {
        int amountOfBases = 20;
        BigInt[] bases = PrimeNumber.generateBasesWithGivenPrimes(amountOfBases, 5000, PrimeNumber.PRIM_BASES);
        System.out.println("Starting search for prime numbers with random bitsize (max: " + maxBitSize + ").");
        System.out.println("For heuristic tests using array containing prim numbers until 37 and " + amountOfBases + " further random bases. Total length: " + bases.length + ".");

        BigInt[] prims = new BigInt[amount];
        for (int i = 0; i < amount; i++) {
            prims[i] = findPrimeForGivenBases(maxBitSize, bases);
        }

        System.out.println("All primes:");
        System.out.println(Arrays.toString(prims));
    }

    static void fermatConditionTesting(int amountOfBases, String path) throws Exception {
        int iterations = 100;
        double cond1 = 0, cond2 = 0;

        BigInt[] numbers = getNumbersFromFile(path);
        numbers = select100Numbers(numbers);
        System.out.println("Selected numbers for tests:");
        System.out.println(Arrays.toString(numbers));

        for (int i = 0; i < iterations; i++) {
            BigInt[] bases = PrimeNumber.generateRandomBaseArray(amountOfBases, 5000);
            //BigInt[] bases = PrimeNumber.PRIMS_UNDER_100;
            boolean[] resultsCond1 = new boolean[iterations];
            boolean[] resultsCond2 = new boolean[iterations];

            for (int j = 0; j < numbers.length; j++) {
                resultsCond1[j] = PrimeNumber.isPrimeFermat(numbers[j], bases);
                resultsCond2[j] = PrimeNumber.isPrimeFermatNoGcd(numbers[j], bases);
                //if (results[j]) System.out.println("number " + numbers[j] + " was identified as prime number.");
            }
            System.out.println(i + "/" + iterations + ": " + countBooleans(resultsCond1) + ", " + countBooleans(resultsCond2));
            cond1 += countBooleans(resultsCond1);
            cond2 += countBooleans(resultsCond2);
        }

        System.out.println("Amount of bases: " + amountOfBases + ", iterations: " + iterations);
        System.out.println("Fermat cond1 false positives: " + cond1/iterations);
        System.out.println("Fermat cond2 false positives: " + cond2/iterations);
    }

    static void millerRabinComparison(int amountOfBases, String path) throws Exception {
        int iterations = 100;
        double average = 0;

        BigInt[] numbers = getNumbersFromFile(path);
        numbers = select100Numbers(numbers);
        System.out.println("Selected numbers for tests:");
        System.out.println(Arrays.toString(numbers));

        for (int i = 0; i < iterations; i++) {
            //BigInt[] bases = PrimeNumber.generateRandomBaseArray(amountOfBases, 5000);
            BigInt[] bases = PrimeNumber.PRIMS_UNDER_100;
            boolean[] results = new boolean[iterations];

            for (int j = 0; j < numbers.length; j++) {
                results[j] = PrimeNumber.isPrimeMR(numbers[j], bases);
                //if (results[j]) System.out.println("number " + numbers[j] + " was identified as prime number.");
            }
            System.out.println(i + "/" + iterations + ": " + countBooleans(results));
            average += countBooleans(results);
        }

        System.out.println("Amount of bases: " + amountOfBases + ", iterations: " + iterations);
        System.out.println("Cases in which Miller Rabin Test identified as prime: " + average/iterations);
    }

    public static void main(String[] args) throws Exception {
        // A) prime numbers
        //String path = "/primes-to-100k.txt";

        // B) pseudo primes
        String path = "/pseudo-primes.txt";

        // C) composite numbers
        //String path = "/composite-numbers-to-10k.txt";

        //comparePrimeNumberTests();
        //findPrimeNumbers(1, 250);
        //millerRabinComparison(19, path);
        fermatConditionTesting(20, path);

    }

    public static BigInt[] getNumbersFromFile(String path) throws Exception {

        Class clazz = FindPrimeNumbers.class;
        InputStream inputStream = clazz.getResourceAsStream(path);

        List<String> numbers = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String lineText;

            while ((lineText = br.readLine()) != null) {
                numbers.add(lineText);
            }

        } catch (IOException ex) {
            System.err.println(ex);
        }

        BigInt[] numbersBigInt = new BigInt[numbers.size()];
        for (int i = 0; i < numbers.size(); i++) {
            numbersBigInt[i] = new BigInt(numbers.get(i));
        }
        return numbersBigInt;
    }

    static BigInt[] select100Numbers(BigInt[] numbers) {
        List<BigInt> arrayList = Arrays.asList(numbers);
        Collections.shuffle(arrayList);

        int amount = 100;
        if (numbers.length < amount)
            amount = numbers.length;

        ArrayList<BigInt> targetList = new ArrayList<>();
        for (int j = 0; j < amount; j++) {
            targetList.add(arrayList.get(j));
        }

        return targetList.toArray(new BigInt[targetList.size()]);
    }

    static int countBooleans(boolean[] array) {
        int count = 0;
        for (boolean a : array) {
            if (a) count++;
        }
        return count;
    }
}

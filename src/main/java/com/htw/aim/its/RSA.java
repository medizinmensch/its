package com.htw.aim.its;

import com.htw.aim.its.domain.RSAprivateKey;
import com.htw.aim.its.domain.RSApublicKey;

public class RSA {

    public static BigInt encryptRSA(RSApublicKey publicKey, BigInt plainData) throws Exception {
        BigInt e = publicKey.getE();
        BigInt n = publicKey.getN();
        BigInt nMinusOne = new BigInt();
        BigInt.sub(n, new BigInt("1"), nMinusOne);

        // plainData > n-1
        if (BigInt.compare(plainData, nMinusOne) == 1)
            throw new Exception("Input block for RSA cant be bigger then N value of key.");

        // Für die Bitkombinationen für block sind einige Werte verboten, u.a. 0, 1 und n-1. Prüfen Sie dies.
        if (!plainData.isGreaterOne() || BigInt.compare(plainData, nMinusOne) == 0) {
            System.out.println("WARN - Given block value was 0, 1 or (n-1), returning without encrypting.");
            return plainData;
        }

        // C ≡ m^e mod n
        BigInt cypherData = BigInt.powerMod(plainData, e, n);

        return cypherData;
    }

    public static BigInt decryptRSA(RSAprivateKey privateKey, BigInt cypherData) throws Exception {
        BigInt d = privateKey.getD();
        BigInt n = privateKey.getN();
        BigInt nMinusOne = new BigInt();
        BigInt.sub(n, new BigInt("1"), nMinusOne);

        // plainData > n-1
        if (BigInt.compare(cypherData, nMinusOne) == 1)
            throw new Exception("Input block for RSA cant be bigger then N value of key.");

        // Für die Bitkombinationen für block sind einige Werte verboten, u.a. 0, 1 und n-1. Prüfen Sie dies.
        if (!cypherData.isGreaterOne() || BigInt.compare(cypherData, nMinusOne) == 0) {
            System.out.println("WARN - Given block value was 0, 1 or (n-1), returning without decrypting.");
            return cypherData;
        }

        // m' ≡ C^d mod n
        BigInt plainData = BigInt.powerMod(cypherData, d, n);
        return plainData;
    }
}

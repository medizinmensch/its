package com.htw.aim.its;

import com.htw.aim.its.domain.RSAprivateKey;
import com.htw.aim.its.domain.RSApublicKey;
import com.htw.aim.its.BigNumber;

public class RSAKeys {

    public static final String BIG_PRIMES_PATH = "/primes-66k-100k.txt";
    private BigInt p;
    private BigInt q;
    private BigInt n;
    private BigInt phi;

    private BigInt e;
    private BigInt d;

    private RSApublicKey publicKey;
    private RSAprivateKey privateKey;

    public RSAKeys(BigInt p, BigInt q, BigInt n, BigInt phi, BigInt e, BigInt d) {
        this.p = p;
        this.q = q;
        this.n = n;
        this.e = e;
        this.d = d;
        this.phi = phi;

        this.publicKey = new RSApublicKey(e, n);
        this.privateKey = new RSAprivateKey(d, n, p, q);
    }

    public RSApublicKey getPublicKey() {
        return this.publicKey;
    }

    public RSAprivateKey getPrivateKey() {
        return this.privateKey;
    }

    public BigInt getPhi() { return this.phi; }


    // allow providing p & q as optional parameters through method overloading
    public static RSAKeys generateRSAKeys(BigInt e, int size) throws Exception {
        return generateRSAKeys(e, size, new BigInt("0"), new BigInt("0"));
    }

    public static RSAKeys generateRSAKeys(BigInt e, int size, BigInt p, BigInt q) throws Exception {
        // conditions:
        // p != q
        // diff(p.length & q.length) < 30 bit
        // n = p*q; n.length >= 2048 (for testing 512 is okay)
        // gcd(e, phi(n)) == 1

        BigInt zero = new BigInt("0");
        boolean reiterate = false;

        if (BigInt.equals(p, zero) && size == 0 || BigInt.equals(q, zero) && size == 0)
            throw new Exception("Please give a size larger then 0 when not providing prime numbers.");
        //if (BigInt.lessThen(e, new BigInt("65536")))
        //    throw new Exception("Given E should be larger then 65536 (2^16+1).");


        while (true) {
            // generate p & q
            BigInt n = new BigInt("0");
            BigInt pMinusOne = new BigInt("0"), qMinusOne = new BigInt("0");
            BigInt phiN = new BigInt("0");
            BigInt ONE = new BigInt("1");

            // check if p & q were provided
            if (BigInt.equals(p, zero))
                p = PrimeNumber.findPrime(size/2+5);
            do {
                if (BigInt.equals(q, zero) || reiterate)
                    q = PrimeNumber.findPrime(size/2+5);

                // calculations
                BigInt.mul(p, q, n);
                BigInt.sub(p, ONE, pMinusOne);
                BigInt.sub(q, ONE, qMinusOne);
                BigInt.mul(pMinusOne, qMinusOne, phiN);
                reiterate = true;
            } while (BigInt.equals(p, q) && BigInt.gcdBin(e, phiN) != ONE);

            BigInt d = BigInt.inversEgcd(e, phiN, phiN);
            return new RSAKeys(p, q, n, phiN, e, d);
        }
    }
}

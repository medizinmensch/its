package com.htw.aim.its;

public class BigIntConfig {

    public final static int DEFAULT_SIZE = 100;
    public final static int MAX_SIZE = 2048;
    public final static int CELL_SIZE = 16;

    final int base = 65536;                 // 2^16
    final int mask = 0xFFFF;                // 2^16 - 1
    final long doubleMask = 0xFFFFFFFFL;    // 2^32 - 1
    final long highBit = 0x8000;
    final int highShift = 15;

    final static long LONG_MASK = 0xffffffffL;
    final static String HEX_DIGITS = "0123456789ABCDEF";
}

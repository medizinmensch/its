package com.htw.aim.its;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

public class Sha256 {

    public long l = 0;
    public int k = 0;
    public int[] W = new int[64];
    public int[] H = new int[8];
    public ArrayList<int[]> debugW = new ArrayList<int[]>();
    public ArrayList<int[]> debugH = new ArrayList<int[]>();
    public ArrayList<String[]> debugR = new ArrayList<String[]>();

    public int[] h = new int[64];
    public String[] R = new String[64];

    static final int[] initial_hash_value = new int[]{
            0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19
    };

    static final int[] K = new int[]{
            0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
            0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
            0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
            0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
            0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
            0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
            0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
            0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
    };

    public int[] sha256(byte[] byteMessage) {
        int[] register = new int[8];
        int[] H = initial_hash_value.clone();

        // make padding START
        l = byteMessage.length * 8; //msg length in bit

        // add '1'
        byteMessage = Arrays.copyOf(byteMessage, byteMessage.length + 1); //increase array size by 1
        byteMessage[byteMessage.length - 1] |= 1 << 7;

        // add k * 0
        while ((l + 1 + k + 64) % 512 != 0) { // messageLengthInBit + 1 + k*0 (padding) + LengthAsLong
            // append i * '0'
            k++;
            if (k % 8 == 0 && k != 0) {
                byteMessage = Arrays.copyOf(byteMessage, byteMessage.length + 1);
            }
        }

        // insert length at the end
        byte shifted = 0;
        for (int i = 7; i >= 0; i--) {
            byteMessage = Arrays.copyOf(byteMessage, byteMessage.length + 1);
            shifted = (byte) ((l >>> i * 8) & 0xff);
            byteMessage[byteMessage.length - 1] = shifted;
        }
        // make padding END

        for (int i = 0; i < byteMessage.length; i += 64) { // = 512 bits (checked)
            int[] M = getBlock(byteMessage, i); // M is always 512 bit long = 16 * 32 bit (16*4 byte)
            W = MSG_Scheduling(M);

            register = H.clone(); // Initial_Register();

            debugH.add(register.clone());
            register = MSG_Compression(register, W);
            H = Compute_Hash_Value(register, H);

            debugW.add(W.clone());
        }
        this.h = H.clone();
        return H;
    }

    static int[] getBlock(byte[] byteMessage, int position) {
        int[] block = new int[16];
        for (int i = 0; i < 16; i++) {
            byte[] arr = {
                    byteMessage[position + 4 * i + 0],
                    byteMessage[position + 4 * i + 1],
                    byteMessage[position + 4 * i + 2],
                    byteMessage[position + 4 * i + 3]
            };
            ByteBuffer wrapped = ByteBuffer.wrap(arr); // big-endian by default
            block[i] = wrapped.getInt();
        }
        return block;
    }

    private int[] MSG_Compression(int[] register, int[] W) {
        int[] T = new int[2];
        for (int i = 0; i < 64; i++) {
            T[0] = register[7] + BSIG1(register[4]) + ch(register[4], register[5], register[6]) + K[i] + W[i]; //  T?!?

            T[1] = BSIG0(register[0]) + maj(register[0], register[1], register[2]);

            register[7] = register[6];
            register[6] = register[5];
            register[5] = register[4];
            register[4] = register[3] + T[0];
            register[3] = register[2];
            register[2] = register[1];
            register[1] = register[0];
            register[0] = T[0] + T[1];

            R[i] = intArrToHex(register);
        }
        debugR.add(R.clone());

        return register; // neccessary as H is ref type?
    }

    int[] MSG_Scheduling(int[] blockM) {
        System.arraycopy(blockM, 0, W, 0, 16);

        for (int i = 16; i < 64; i++) {
            W[i] = SSIG1(W[i - 2]) + W[i - 7] + SSIG0(W[i - 15]) + W[i - 16];
        }

        // hex(W[0] - W[15]) should be 128 chars long
        return W;
    }

    public static String intArrToHex(int[] arr) {
        StringBuilder text = new StringBuilder("+0x");
        for (int b : arr) {
            text.append(String.format("%08x", b));
        }
        return text.toString();
    }

    private static int[] Compute_Hash_Value(int[] register, int[] H) {
        for (int i = 0; i < 8; i++) {
            H[i] = register[i] + H[i];
        }
        return H;
    }

    static int ch(int x, int y, int z) {
        return (x & y) ^ (~x & z); // (x AND y) XOR ((NOT x) AND z)
    }

    static int maj(int x, int y, int z) {
        return (x & y) ^ (x & z) ^ (y & z); // (x AND y) XOR (x AND z) XOR (y AND z)
    }

    static int BSIG0(Integer x) {
        return Integer.rotateRight(x, 2) ^ Integer.rotateRight(x, 13) ^ Integer.rotateRight(x, 22);
    }

    static int BSIG1(Integer x) {
        return Integer.rotateRight(x, 6) ^ Integer.rotateRight(x, 11) ^ Integer.rotateRight(x, 25);
    }

    static int SSIG0(Integer x) {
        return Integer.rotateRight(x, 7) ^ Integer.rotateRight(x, 18) ^ x >>> 3;
    }

    static int SSIG1(Integer x) {
        return Integer.rotateRight(x, 17) ^ Integer.rotateRight(x, 19) ^ x >>> 10;
    }
}

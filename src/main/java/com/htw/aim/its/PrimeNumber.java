package com.htw.aim.its;

import java.util.Arrays;
import java.util.Random;

import static com.htw.aim.its.BigIntConfig.CELL_SIZE;

public class PrimeNumber {
    public static final BigInt[] PRIM_BASES = getBigIntArray(new int[] {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37});
    public static final BigInt[] PRIMS_UNDER_100 =
            getBigIntArray(new int[] {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43,
                    47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97});

    static final BigInt ONE = new BigInt(1);
    static final BigInt TWO = new BigInt(2);
    static Random random = new Random();

    static BigInt[] getBigIntArray(int[] numbers) {
        BigInt[] bigInts = new BigInt[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            bigInts[i] = new BigInt(numbers[i]);
        }
        return bigInts;
    }

    public static BigInt[] generateRandomBaseArray(int amount, int max) {
        BigInt[] randomBases = new BigInt[amount];
        for (int i = 0; i < amount; i++) {
            BigInt randomInt = new BigInt(random.nextInt(max));

            // check if randomNumber is in arrays
            while (PrimeNumber.contains(PRIM_BASES, randomInt) || PrimeNumber.contains(randomBases, randomInt) || randomInt.isZero())
                randomInt = new BigInt(random.nextInt(max));
            randomBases[i] = randomInt;
        }
        return randomBases;
    }

    static BigInt[] generateBasesWithGivenPrimes(int amountOfBases, int highestBaseValue, BigInt[] primBases) {
        // generate bases & combine with given bases
        BigInt[] randomBases = PrimeNumber.generateRandomBaseArray(amountOfBases, highestBaseValue);
        BigInt[] bases = new BigInt[randomBases.length + primBases.length];
        System.arraycopy(randomBases, 0, bases, 0, randomBases.length);
        System.arraycopy(primBases, 0, bases, randomBases.length, primBases.length);

        return bases;
    }

    public static BigInt getRandomOdd(int size) throws Exception {
        if (size < 3)
            throw new Exception("Cannot create random odd numbers < 3");

        BigInt randomOdd = new BigInt(0);
        randomOdd.spart = 0;
        StringBuilder bitstring = new StringBuilder();
        bitstring.append(1);

        for (int i = 1; i < size - 1; i++) {
            bitstring.append(random.nextBoolean() ? 0 : 1);

            if (bitstring.length() == CELL_SIZE) {
                randomOdd.values[randomOdd.spart] = Integer.parseInt(bitstring.reverse().toString(), 2);
                randomOdd.spart++;
                bitstring.setLength(0);
            }
        }
        bitstring.append(1);

        randomOdd.values[randomOdd.spart] = Integer.parseInt(bitstring.reverse().toString(), 2);
        randomOdd.spart++;
        bitstring.setLength(0);

        // System.out.println("Created random odd number '" + randomOdd + "', in bits: " + randomOdd.toBinary());
        return randomOdd;
    }

    public static BigInt findPrime(int bitLength) throws Exception {
        return findPrimeNotEquals(bitLength, new BigInt(0));
    }

    public static BigInt findPrimeNotEquals(int bitLength, BigInt notEquals) throws Exception {
        bitLength = bitLength + (int) (Math.random() * 8) - 4; // from -4 to 4
        BigInt number = PrimeNumber.getRandomOdd(bitLength);
        BigInt[] bases = generateBasesWithGivenPrimes(30, 5000, PRIMS_UNDER_100);

        // todo vary bitLength +-1?
        while (!PrimeNumber.isPrime(number, bases) && !BigInt.equals(number, notEquals))
            number = PrimeNumber.getRandomOdd(bitLength);

        //System.out.println(" ---------------- Found prime number: '" + number + "'.  ---------------- ");
        return number;
    }

    public static BigInt findPrimeLinearlyWithStartValue(BigInt startValue) throws Exception {
        BigInt number = new BigInt("0");
        if (!startValue.isOdd())
            BigInt.add(startValue, ONE, number);
        else
            BigInt.add(startValue, TWO, number);

        BigInt[] bases = generateBasesWithGivenPrimes(30, 5000, PRIMS_UNDER_100);

        while (!PrimeNumber.isPrime(number, bases)) {
            BigInt.add(number, TWO, number);
        }
        //System.out.println("INFO - Generated prime number: " + number);
        return number;
    }

    private static int basicPrimeCheck(BigInt prim) {
        // 0: no conclusion
        // 1: no prim
        // 2: is prim
        BigInt.reduce(prim);
        if (prim.spart > 1)
            return 0;

        if (prim.isZero() || !prim.isGreaterOne())
            return 1;
        if (BigInt.equals(prim, new BigInt(2)))
            return 2;
        if (prim.isEven())
            return 1;

        for (BigInt basicBase : PRIM_BASES) {
            if (BigInt.equals(basicBase, prim))
                return 2;
        }
        return 0;
    }


    // all combining prime check
    static boolean isPrime(BigInt number, BigInt[] bases) throws Exception {
        number.positive = true;
        //System.out.println("Testing number for prim. Value: " + number);

        // basic check
        int conclusion = basicPrimeCheck(number);
        if (conclusion == 1) return false;
        if (conclusion == 2) return true;

        //check if given number is known prime
        for (BigInt prime : PrimeNumber.PRIMS_UNDER_100) {
            if (BigInt.equals(prime, number))
                return true;
        }

        // div test with prims under 100
        if (!PrimeNumber.isPrimeDiv(number, PrimeNumber.PRIMS_UNDER_100))
            return false;
        //System.out.println("Current number survived 'Div-Test'.");

        // not necessary?
        //// fermat test
        //if (!PrimeNumber.isPrimeFermat(number, bases))
        //    return false;
        //System.out.println("Current number '" + number + "' survived 'Fermat-Test'.");

        //// euler test
        //if (!PrimeNumber.isPrimeFermat(number, bases))
        //    return false;
        //System.out.println("Current number '" + number + "' survived 'Euler-Test'.");

        // MR test
        if (!PrimeNumber.isPrimeMR(number, bases))
            return false;
        //System.out.println("Current number survived 'MillerRabin-Test'.");
        return true;
    }


    // --------------- single base ---------------
    // methods return true if number is prim, false if number is no prim
    public static boolean isPrimeFermatNoGcd(BigInt prim, BigInt base) throws Exception {
        // condition: a^n = a (mod n) with a>0 and n is prim
        BigInt possiblePrim = (BigInt) prim.clone();

        BigInt rest = BigInt.powerMod(base, possiblePrim, prim);
        return BigInt.equals(rest, base);
    }

    public static boolean isPrimeFermat(BigInt prim, BigInt base) throws Exception {
        // condition: a^(n-1) = 1 (mod n) with a>0, gcd(a,n) = 1 and n is prim
        BigInt possiblePrim = (BigInt) prim.clone();

        if (!BigInt.equals(prim, base) && BigInt.gcdBin(base, possiblePrim).isGreaterOne()) {
            return false;
        }
        BigInt.sub(possiblePrim, ONE, possiblePrim);
        BigInt rest = BigInt.powerMod(base, possiblePrim, prim);
        return BigInt.equals(rest, ONE);
    }

    public static boolean isPrimeEulerNoGcd(BigInt prim, BigInt base) throws Exception {
        // condition: a^((n-1)/2) = +-1 (mod n)
        BigInt possiblePrim = (BigInt) prim.clone();

        BigInt.sub(possiblePrim, ONE, possiblePrim);
        BigInt.shiftRight(possiblePrim);
        BigInt rest = BigInt.powerMod(base, possiblePrim, prim);

        BigInt primMinusOne = new BigInt(0);
        BigInt.sub(prim, ONE, primMinusOne);
        if (BigInt.equals(rest, ONE) || BigInt.equals(rest, primMinusOne))
            return true;

        return false;
    }

    public static boolean isPrimeEuler(BigInt prim, BigInt base) throws Exception {
        // condition: a^((n-1)/2) = +-1 (mod n)
        BigInt possiblePrim = (BigInt) prim.clone();
        if (!BigInt.equals(prim, base) && BigInt.gcdBin(base, possiblePrim).isGreaterOne())
            return false;

        BigInt.sub(possiblePrim, ONE, possiblePrim);
        BigInt.shiftRight(possiblePrim);
        BigInt rest = BigInt.powerMod(base, possiblePrim, prim);

        BigInt primMinusOne = new BigInt(0);
        BigInt.sub(prim, ONE, primMinusOne);
        if (BigInt.equals(rest, ONE) || BigInt.equals(rest, primMinusOne))
            return true;

        return false;
    }

    public static boolean isPrimeMR(BigInt prim, BigInt base) throws Exception {
        if (BigInt.equals(prim, new BigInt(2)))
            return true;

        BigInt possiblePrim = (BigInt) prim.clone();
        if (!BigInt.equals(prim, base) && BigInt.gcdBin(base, possiblePrim).isGreaterOne())
            return false;

        // make even
        BigInt primMinusOne = new BigInt(0);
        BigInt.sub(prim, ONE, primMinusOne);

        BigInt d = (BigInt) primMinusOne.clone();
        int s = 0;

        // reduce d
        while (d.isEven()) {
            BigInt.shiftRight(d);
            s += 1;
        }

        BigInt rest = BigInt.powerMod(base, d, prim);
        if (BigInt.equals(rest, ONE) || BigInt.equals(rest, new BigInt("-1")) || BigInt.equals(rest, primMinusOne))
            return true;
        else {
            for (int i = 0; i < s; i++) {
                BigInt.shiftLeft(d, 1);
                rest = BigInt.powerMod(base, d, prim);
                if (BigInt.equals(rest, new BigInt("-1")) || BigInt.equals(rest, primMinusOne))
                    return true;
                if (BigInt.equals(rest, ONE))
                    return false;
            }
            return false;
        }
    }

    // --------------- multiple bases ---------------

    public static boolean isPrimeDiv(BigInt prim, BigInt[] bases) throws Exception {
        // test simple division prim/bases, if modulo == 0 --> false
        for (BigInt base : bases) {
            if (BigInt.equals(prim, base)) {
                System.out.println("WARN - prim number was same as used base - " + prim);
                // what to do here?
                return true;
            }

            if (Arrays.asList(PRIMS_UNDER_100).contains(prim))
                return true;

            BigInt result = new BigInt(0);
            BigInt rest = BigInt.divmod(prim, base, result);
            if (rest.isZero())
                return false;
        }
        return true;
    }

    public static boolean isPrimeFermat(BigInt prim, BigInt[] bases) throws Exception {
        for (int i = 0; i < bases.length; i++) {
            BigInt base = bases[i];
            if (!isPrimeFermat((BigInt) prim.clone(), base))
                return false;
        }
        return true;
    }

    public static boolean isPrimeFermatNoGcd(BigInt prim, BigInt[] bases) throws Exception {
        for (int i = 0; i < bases.length; i++) {
            BigInt base = bases[i];
            if (!isPrimeFermatNoGcd((BigInt) prim.clone(), base))
                return false;
        }
        return true;
    }

    public static boolean isPrimeEuler(BigInt prim, BigInt[] bases) throws Exception {
        for (int i = 0; i < bases.length; i++) {
            BigInt base = bases[i];
            if (!isPrimeEuler(prim, base))
                return false;
        }
        return true;
    }

    public static boolean isPrimeMR(BigInt prim, BigInt[] bases) throws Exception {
        for (int i = 0; i < bases.length; i++) {
            BigInt base = bases[i];
            if (!isPrimeMR(prim, base))
                return false;
        }
        return true;
    }

    public static boolean contains(final BigInt[] array, final BigInt key) {
        return Arrays.asList(array).contains(key);
    }

}

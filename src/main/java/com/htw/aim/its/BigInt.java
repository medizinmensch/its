package com.htw.aim.its;

import com.htw.aim.its.domain.EgcdTuple;
import java.util.Date;

public class BigInt extends BigNumber {

    BigInt() {
        super();
    }

    public BigInt(String str) throws Exception {
        super(str);
    }

    BigInt(int value) {
        super(value);
    }

    public BigInt(int size, String str) throws Exception {
        super(size, str);
    }

    BigInt(int size, int value) {
        super(size, value);
    }

    // --- public arithmetic functions with sign ---
    public static void add(BigInt a, BigInt b, BigInt result) throws Exception {
        switch (lookAtSign(a, b)) {
            case POSITIVE_POSITIVE:
                _add(a, b, result, true);
                return;
            case NEGATIVE_POSITIVE:
                // a <= b
                if (compareAbsoluteValues(a, b) <= 0) _sub(b, a, result, true);
                else _sub(a, b, result, false);
                return;
            case POSITIVE_NEGATIVE:
                if (compareAbsoluteValues(a, b) <= 0) _sub(b, a, result, false);
                else _sub(a, b, result, true);
                return;
            case NEGATIVE_NEGATIVE:
                _add(a, b, result, false);
        }
    }

    public static void sub(BigInt a, BigInt b, BigInt result) throws Exception {
        switch (lookAtSign(a, b)) {
            case POSITIVE_POSITIVE:
                if (compareAbsoluteValues(a, b) <= 0) _sub(b, a, result, false);
                else _sub(a, b, result, true);
                return;
            case NEGATIVE_POSITIVE:
                _add(a, b, result, false);
                return;
            case POSITIVE_NEGATIVE:
                _add(a, b, result, true);
                return;
            case NEGATIVE_NEGATIVE:
                if (compareAbsoluteValues(a, b) <= 0) _sub(b, a, result, true);
                else _sub(a, b, result, false);
        }
    }

    public static void mul(BigInt a, BigInt b, BigInt result) throws Exception {
        switch (lookAtSign(a, b)) {
            case POSITIVE_POSITIVE:
            case NEGATIVE_NEGATIVE:
                _mul(a, b, result, true);
                return;
            case NEGATIVE_POSITIVE:
            case POSITIVE_NEGATIVE:
                _mul(a, b, result, false);
                return;
        }
    }

    public static BigInt divmod(BigInt dividend, BigInt divisor, BigInt result) throws Exception {
        BigInt remainder;
        switch (lookAtSign(dividend, divisor)) {
            case POSITIVE_POSITIVE:
                return _divmod(dividend, divisor, result, true);
            case NEGATIVE_NEGATIVE:
                remainder = _divmod(dividend, divisor, result, true);
                if (remainder.isNotZero()) {
                    BigInt returnable = new BigInt(0);
                    add(result, new BigInt(1), result);
                    sub(divisor, remainder, returnable);
                    return returnable;
                }
                return remainder;
            case NEGATIVE_POSITIVE:
                remainder = _divmod(dividend, divisor, result, false);
                if (remainder.isNotZero()) {
                    BigInt returnable = new BigInt(0);
                    sub(result, new BigInt(1), result);
                    sub(divisor, remainder, returnable);
                    return returnable;
                }
                return remainder;
            case POSITIVE_NEGATIVE:
                remainder = _divmod(dividend, divisor, result, false);
                return remainder;
        }
        throw new Exception("could not resolve Sign");
    }

    // --- private arithmetic functions without sign ---

    private static void _add(BigInt a, BigInt b, BigInt result, boolean positive) throws Exception {
        if (a.isZero()) {
            result.copyFromBigInt(b);
            return;
        }
        if (b.isZero()) {
            result.copyFromBigInt(a);
            return;
        }

        int tmp, over = 0;
        BigNumber.sameSize(a, b);

        int i = 0;
        // skip zeros in the beginning
        while (a.values[i] == 0 && b.values[i] == 0)
            i++;

        // do calculations for remaining array values
        for (; i < a.spart; i++) {
            tmp = a.values[i] + b.values[i] + over;
            result.values[i] = tmp % a.base;
            over = tmp / a.base; // 0 or 1
        }
        if (over > 0) {
            BigNumber.resize(result, a.spart + 1);
            result.values[a.spart] = over;
        }
        result.positive = positive;
        reduce(result);
    }

    private static void _sub(BigInt a, BigInt b, BigInt result, boolean positive) throws Exception {
        // a!<=b
        int tmp, over = 0;
        BigNumber.sameSize(a, b);
        resize(result, a.spart + 1);

        for (int i = 0; i < a.spart; i++) {

            tmp = a.values[i] - b.values[i] + over;
            over = (short) ((tmp >> CELL_SIZE) & a.mask);
            if (tmp < 0)    // entmaskieren
                tmp = tmp & a.mask;
            result.values[i] = tmp % a.base;

        }
        result.values[a.spart] = over;
        result.positive = positive;
        reduce(result);

    }

    private static void _mul(BigInt a, BigInt b, BigInt result, boolean positive) throws Exception {
        if (a == result)
            a = (BigInt) a.clone();
        if (b == result)
            b = (BigInt) b.clone();

        // small hack to make sure result is zero for further calculation and address stays the same
        sub(result, result, result); // sub & add are safe to insert same BigInt's

        BigNumber.sameSize(a, b);
        resize(result, 2 * a.spart + 1);

        for (int i = 0; i < b.spart; i++) {
            for (int j = 0; j < a.spart; j++) {
                long tmp = a.values[j] * b.values[i];
                if (tmp < 0) tmp = (tmp & a.doubleMask);        // demaskieren
                if (tmp > 0) addCell2(result, i + j, tmp);
            }
        }
        result.positive = positive;
        BigInt.reduce(result);
    }

    private static BigInt _divmod(BigInt dividend_a, BigInt divisor_b, BigInt result, boolean positive) throws Exception {
        reduce(dividend_a);
        reduce(divisor_b);

        // from this point on only use positive numbers
        dividend_a.positive = true;
        divisor_b.positive = true;

        // small hack to make sure result is zero for further calculation and address stays the same
        sub(result, result, result); // sub & add are safe to insert same BigInt's

        // check all special cases
        if (divisor_b.isZero()) throw new Exception("ERROR - Divide by Zero is not possible.");

        if ((dividend_a.spart < divisor_b.spart) || (dividend_a.spart == divisor_b.spart && BigInt.compareAbsoluteValues(dividend_a, divisor_b) == -1)) {
            return (BigInt) dividend_a.clone();
        }

//        if (divisor_b.spart == 0) {
//            //divisor_b.positive = true;
//            int divisor = divisor_b.values[0];
//            return divByCell(dividend_a, divisor, result);
//        }

//         if small enough, we can use simple 32/64 bit division
//        if (dividend_a.spart <= 1 && divisor_b.spart <= 1) {
//            int res = dividend_a.values[0] / divisor_b.values[0];
//            result.values[0] = res;
//            return new BigInt (dividend_a.values[0] % divisor_b.values[0]);
//        }

//        BigInt remainder_r = (BigInt)dividend_a.clone();
        BigInt remainder_r = new BigInt(0);


        remainder_r.positive = true;

        int divisor_b_spart_K = divisor_b.spart;
        int spart_diff_L = dividend_a.spart - divisor_b.spart;

        // copy first few values.
        for (int i = 0; i < divisor_b_spart_K; i++) {
            remainder_r.values[i] = dividend_a.values[spart_diff_L + i];
            reduce(remainder_r);
        }
        for (int i = spart_diff_L; i >= 0; i--) {
            int e = estimate(remainder_r.values[remainder_r.spart - 1], remainder_r.values[remainder_r.spart], divisor_b.values[divisor_b.spart - 1]);
            BigInt tmp_backmultiplication = mulCell(divisor_b, e);

            if (compareAbsoluteValues(tmp_backmultiplication, remainder_r) != 0) {
                e = downsizeEBySteps(tmp_backmultiplication, divisor_b, e, remainder_r);
                e = enlargeEByStepsFastAltSecond(tmp_backmultiplication, divisor_b, e, remainder_r);
            }
            result.values[i] = e; // improve for negative numbers and for really high numbers

            sub(remainder_r, tmp_backmultiplication, remainder_r);

            if (i != 0) { // shift left 1 cell
                mul(remainder_r, new BigInt(dividend_a.base), remainder_r);
                add(remainder_r, new BigInt(dividend_a.values[i - 1]), remainder_r);
            }
        }
        result.positive = positive;
        reduce(result);
        reduce(remainder_r);
        return remainder_r;
    }

    private static int enlargeEBySteps(BigInt tmp_backmultiplication, BigInt divisor_b, int e, BigInt remainder_r) throws Exception {
        BigInt r_tmp_diff = new BigInt(0);
        sub(remainder_r, tmp_backmultiplication, r_tmp_diff);
        Date date = new Date(System.currentTimeMillis());
        while (compareAbsoluteValues(r_tmp_diff, divisor_b) >= 0) { // while(r-tmp >= b) e++
            e = e + 1;
            add(tmp_backmultiplication, divisor_b, tmp_backmultiplication);
        }
        return e;
    }

    private static int enlargeEByStepsFastAlt(BigInt tmp_backmultiplication, BigInt b, int e, BigInt r) throws Exception {
        int factor = 1;
        while (true) { //21474836487 = max for signed int //todo
            e += factor; // increase e

            BigInt diff = new BigInt(MAX_SIZE, 0); // tmp for comparison in if
            mul(new BigInt(e), b, diff);
            sub(r, diff, diff); // == (r - e * b)

            if (compare(diff, b) <= 0) {// == if (r - e * b > b)  // overshot detected
                if (factor == 1 || compare(diff, new BigInt(0)) == 0) { // maximum e reached
                    mul(b, new BigInt(e), tmp_backmultiplication); // set tmp
                    return e;
                }
                e -= factor; // otherwise, set e back
                factor = 1; // restart at the beginning
            } else factor *= 2;
        }
        // todo, catch diff == 0 case
    }

    private static int enlargeEByStepsFastAltSecond(BigInt tmp_backmultiplication, BigInt b, int e, BigInt r) throws Exception {
        int factor = 1;
        BigInt diff = new BigInt(0);
        BigInt zero = new BigInt(0);
        while (true) {
            mul(b, new BigInt(e), tmp_backmultiplication);
            sub(r, tmp_backmultiplication, diff);

            if (compare(diff, b) >= 0) { // increase e
                e += factor;
                factor *= 2;
            } else if (compare(diff, zero) < 0) { //decrease e
                e -= (factor/2); // otherwise, set e back
                factor = 1; // restart at the beginning
            } else if (compare(diff, zero) == 0 || (compare(diff, b) < 0 && compare(diff, zero) > 0)){ //return e
                return e;
            } else {
                System.out.println("The program shouldn't be here. This case shouldn't exist...");
            }
        }
        // todo, catch diff == 0 case
    }

    private static int downsizeEBySteps(BigInt tmp_backmultiplication, BigInt divisor_b, int e, BigInt remainder_r) throws Exception {
        while (compareAbsoluteValues(tmp_backmultiplication, remainder_r) > 0) {
            e = e - 1;
            sub(tmp_backmultiplication, divisor_b, tmp_backmultiplication);
        }
        return e;
    }

    // --- special cases for arithmetic calculations ---

    static void addCell2(BigInt a, int index, long b) throws Exception {
        BigInt.resize(a, a.spart + 1);
        long tmp, over = b;
        while (over != 0) {
            tmp = a.values[index] + over % a.base;
            a.values[index] = (int) tmp % a.base;
            over = tmp / a.base + over / a.base;
            index++;
        }
        BigInt.reduce(a);
    }

    static BigInt mulCell(BigInt a, int b) throws Exception {
        BigInt result = new BigInt(MAX_SIZE, 0);
        resize(result, a.spart + 1);
        long tmp;
        for (int i = 0; i < a.spart; i++) {
            tmp = a.values[i] * b;
            BigInt.addCell2(result, i, tmp);
        }
        return result;
    }

    static BigInt mul8(BigInt a) throws Exception {
        shiftLeft(a, 3);     // c= c*16 (16c)
        return a;
    }

    static BigInt mul10(BigInt a) throws Exception {
        BigInt c = (BigInt) a.clone();
        shiftLeft(c, 2);     // c= c*4 (4c)
        add(a, c, c);            // c= c+a (5c)
        shiftLeft(c, 1);    // c= c*2 (10c)
        return c;
    }

    static BigInt mul16(BigInt a) throws Exception {
        shiftLeft(a, 4);     // c= c*16 (16c)
        return a;
    }

    static int divmod10(BigInt a) throws Exception {
        BigInt ten = new BigInt(10);
        BigInt tmp = (BigInt) a.clone();
        BigInt rest = _divmod(tmp, ten, a, a.positive);
        return rest.values[0];      // rest can never be higher then 9 so we return only int
    }

    static BigInt divByCell(BigInt dividend_a, int divisor, BigInt result) throws Exception {
        if (divisor == 0) {
            throw new Exception("ERROR - Can't divide by 0. (in divByCell method)");
        }
        String over = "0";
        resize(result, dividend_a.spart);
        for (int i = dividend_a.spart - 1; i >= 0; i--) {
            over += "0";
            int current_divident = Integer.parseInt(over) + dividend_a.values[i];
            if (current_divident < divisor) {
                over = current_divident + "";
                continue;
            }
            result.values[i] = current_divident / divisor;
            over = current_divident % divisor + "";
        }
        reduce(result);
        return new BigInt(over);
    }

    // --- further calculation methods ---

    static void shiftLeft(BigInt a, int cnt) throws Exception {
        shiftLeftWithResize(a, cnt, true);
    }

    static void shiftLeftWithResize(BigInt a, int cnt, boolean resize) throws Exception {
        if (cnt >= a.base) {
            throw new Exception("cnt (" + cnt + ") bigger/equals base (" + a.base + ") value. cnt must be smaller!");
            // Note: I don't get why, but it's in his code...
        }
        int over = 0;
        //BigInt.resize(a, a.spart + 1);
        for (int i = 0; i < a.spart; i++) {
            int tmp = (a.values[i] << cnt) + over;
            a.values[i] = tmp % a.base;
            over = tmp / a.base;
        }

        // length check necessary!
        if (over > 0) {
            // resizing bigint
            if (resize && a.size != MAX_SIZE && a.spart >= a.size) {
                a.size = MAX_SIZE;
                int[] biggerValues = new int[MAX_SIZE];
                System.arraycopy(biggerValues, 0, a.values, 0, a.values.length);
                a.values = biggerValues;
            }
            else if (a.spart >= a.size) {
                throw new Exception("Shifting left would result in larger number then maximum size '" + a.size + "'.");
            }
            a.values[a.spart] = over;
            a.spart++;
        }
        reduce(a);
    }

    static int shiftRight(BigInt a) {
        int lowest = a.values[0] & 1;
        for (int i = 0; i < a.spart - 1; i++) {
            int low = a.values[i + 1] & 1;
            a.values[i] = (int) (((a.values[i] >>> 1) & ~a.highBit) | (low << a.highShift));
        }
        a.values[a.spart - 1] = (a.values[a.spart - 1] >>> 1);
        reduce(a);
        return lowest;
    }

    static BigInt power(BigInt a, BigInt exponent) throws Exception {
        if (a.isZero() && exponent.isZero()) return new BigInt(1);
        if (exponent.isZero()) return new BigInt(1);
        if (a.isZero()) return new BigInt(0);


        BigInt result = new BigInt(MAX_SIZE, 1);
        resize(result, a.spart * exponent.spart + exponent.spart);

        // non destructive exponent
        BigInt n = (BigInt) exponent.clone();

        BigInt tmp = (BigInt) a.clone();
        while (n.isNotZero()) {
            if (n.isOdd()) {
                BigInt.mul(result, tmp, result);
            }
            tmp = BigInt.square(tmp);
            shiftRight(n);
        }
        reduce(result);
        return result;
    }

    public static BigInt powerMod(BigInt a, BigInt n, BigInt modul) throws Exception {
        if (modul.isZero()) {
            throw new Exception("Modul can't be zero");
        }
        if (n.isZero()) return new BigInt(1);

        BigInt exponent = (BigInt) n.clone(); // non destructive
        if (a == modul)
            a = (BigInt) a.clone();


        BigInt tmp = (BigInt) a.clone();
        BigInt result = new BigInt(MAX_SIZE, 1);
        while (exponent.isNotZero()) {
            BigInt current = new BigInt(MAX_SIZE, 0);
            if (exponent.isOdd()) {
                mul(result, tmp, current);
                result = divmod(current, modul, new BigInt(0));
            }
            tmp = square(tmp);
            tmp = divmod(tmp, modul, current);
            shiftRight(exponent);
        }
        return result;
    }

    static BigInt square(BigInt a) throws Exception {
        BigInt result = new BigInt(0);
        long tmp;
        resize(result, 2 * a.spart + 1);

        for (int i = 0; i < a.spart; i++) {
            for (int j = i + 1; j < a.spart; j++) {
                tmp = a.values[i] * a.values[j];
                if (tmp < 0) tmp = (tmp & a.doubleMask);        // demaskieren
                addCell2(result, i + j, tmp);
                addCell2(result, i + j, tmp);
            }
            tmp = a.values[i] * a.values[i];
            if (tmp < 0) tmp = (tmp & a.doubleMask);        // demaskieren
            addCell2(result, 2 * i, tmp);
        }
        return result;
    }

    public static BigInt gcdBin(BigInt a, BigInt b) throws Exception {
        if (a.isZero() || b.isZero())
            throw new Exception("Values for gcd can't be zero...");

        // non destructive & positive values only
        BigInt aPos = (BigInt) a.clone();
        aPos.positive = true;
        BigInt bPos = (BigInt) b.clone();
        bPos.positive = true;

        // optimized: "while (aPos.isEven() && bPos.isEven())"
        int k = Math.min(zeroBits(aPos), zeroBits(bPos));
        for (int i = 0; i < k; i++) {
            shiftRight(aPos);
            shiftRight(bPos);
        }

        while (aPos.isNotZero()) {
            while (aPos.isEven())
                shiftRight(aPos);
            while (bPos.isEven())
                shiftRight(bPos);
            if (BigInt.compareAbsoluteValues(aPos, bPos) == -1) {       // if a<b --> exchange a & b
                BigInt tmp = (BigInt) aPos.clone();
                aPos = (BigInt) bPos.clone();
                bPos = (BigInt) tmp.clone();
            }
            sub(aPos, bPos, aPos);
        }

        BigInt result = (BigInt) bPos.clone();
        shiftLeft(result, k);
        return result;
    }

    static EgcdTuple egcdBin(BigInt a, BigInt b) throws Exception {
        if (a.isZero() || b.isZero())
            throw new Exception("Values for gcd can't be zero...");

        // non destructive & positive values only
        BigInt aPos = (BigInt) a.clone();
        aPos.positive = true;
        BigInt bPos = (BigInt) b.clone();
        bPos.positive = true;

        // optimized: "while (aPos.isEven() && bPos.isEven())"
        int k = Math.min(zeroBits(aPos), zeroBits(bPos));
        for (int i = 0; i < k; i++) {
            shiftRight(aPos);
            shiftRight(bPos);
        }

        BigInt au = new BigInt(1), av = new BigInt(0);
        BigInt bu = new BigInt(0), bv = new BigInt(1);
        BigInt x = (BigInt) aPos.clone();
        BigInt y = (BigInt) bPos.clone();

        while (x.isNotZero()) {
            while (x.isEven()) {
                shiftRight(x);
                if (x.isEven() && y.isEven()) {
                    shiftRight(au);
                    shiftRight(av);
                } else {
                    add(au, bPos, au); // au:= (au+b)/2;
                    shiftRight(au);
                    sub(av, aPos, av); // av:= (av-a)/2;
                    shiftRight(av);
                }
            }
            while (y.isEven()) {
                shiftRight(y);
                if (x.isEven() && y.isEven()) {
                    shiftRight(bu);
                    shiftRight(bv);
                } else {
                    add(bu, aPos, bu); // bu:= (bu+a)/2;
                    shiftRight(bu);
                    sub(bv, bPos, bv); // bv:= (bv-b)/2;
                    shiftRight(bv);
                }
            }
            if (lessThen(x, y)) {
                sub(y, x, y);
                sub(bu, au, bu);
                sub(bv, av, bv);
            } else {
                sub(x, y, x);
                sub(au, bu, au);
                sub(av, bv, av);
            }
        }
        shiftLeft(y, k);
        return new EgcdTuple(y, bu, bv);
    }

    static EgcdTuple egcdMod(BigInt a, BigInt b) throws Exception {
        if (a.isZero() || b.isZero())
            throw new Exception("Values for gcd can't be zero...");

        // non destructive & positive values only
        BigInt aPos = (BigInt) a.clone();
        aPos.positive = true;
        BigInt bPos = (BigInt) b.clone();
        bPos.positive = true;

        BigInt au = new BigInt(1), av = new BigInt(0);
        BigInt bu = new BigInt(0), bv = new BigInt(1);

        while (bPos.isNotZero()) {
            BigInt q = new BigInt(0);
            BigInt t = BigInt.divmod(aPos, bPos, q);
            BigInt tu = new BigInt(0);  // tu:= au-q*bu;
            BigInt.mul(q, bu, tu);
            BigInt.sub(au, tu, tu);

            BigInt tv = new BigInt(0);  // tv:= av-q*bv;
            BigInt.mul(q, bv, tv);
            BigInt.sub(av, tv, tv);

            aPos = (BigInt) bPos.clone();
            au = (BigInt) bu.clone();
            av = (BigInt) bv.clone();

            bPos = (BigInt) t.clone();
            bu = (BigInt) tu.clone();
            bv = (BigInt) tv.clone();
        }
        return new EgcdTuple(aPos, au, av);
    }

    public static BigInt inversEgcd(BigInt e, BigInt phiN, BigInt modul) throws Exception {
        EgcdTuple tuple = egcdMod(e, phiN);
        if (!BigInt.equals(tuple.getGcd(), new BigInt(1)))
            throw new Exception("GCD for e and Phi(n) was not 1 but should be.");

        BigInt d = tuple.getU();
        if (!d.positive)
            BigInt.add(d, phiN, d);
        return d;
    }

    public int bitLength() {
        return this.spart*16;
    }
}

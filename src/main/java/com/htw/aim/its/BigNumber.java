package com.htw.aim.its;

import com.htw.aim.its.domain.SignRelation;

import static com.htw.aim.its.domain.SignRelation.*;

public class BigNumber extends BigIntConfig implements Cloneable {
    int size;
    int spart;
    boolean positive;
    int[] values = new int[size]; // Java int's are by default 32bit, but signed


    BigNumber() {
        this(0);
    }

    BigNumber(String str) throws Exception {
        this.size = MAX_SIZE;
        toBigInt(str);
    }

    BigNumber(int bitsize, String str) throws Exception {
        this(bitsize, 0);
        toBigInt(str);
    }

    BigNumber(int val) {
        this(DEFAULT_SIZE, val);
    }

    BigNumber(int bitsize, int val) {
        this.size = bitsize;
        this.spart = 1;
        this.positive = true;
        this.values = new int[this.size];
        this.values[0] = val;
    }

    private void toBigInt(String word) throws Exception {

        if (word == null || word.equals(""))
            throw new Exception("Given string for BigInt was 'null' or \"\"");

        // set sign
        if (word.charAt(0) == '-') {
            this.positive = false;
            word = word.substring(1);
        } else if (word.charAt(0) == '+') {
            this.positive = true;
            word = word.substring(1);
        } else this.positive = true;

        // remove unnecessary chars
        word = word.replaceAll("[^0-9]", "");

        if (word.length() < 1)
            throw new Exception("Given word has no digits.");

        if (word.length() > MAX_SIZE) {
            throw new Exception("Given number is longer then maximum size (" + MAX_SIZE + ").");
        }

        // read string into big int
        BigInt c = new BigInt(MAX_SIZE, charToDigit(word.charAt(0)));
        c.spart = 1;

        for (int i = 1; i < word.length(); i++) {
            BigInt digit = new BigInt(MAX_SIZE, charToDigit(word.charAt(i)));
            BigInt.add(BigInt.mul10(c), digit, c);
        }

        reduce(c);
        this.values = c.values.clone();
        this.spart = c.spart;
    }

    public static BigInt toBigIntFromHex(String word) throws Exception {
        return toBigIntFromHexWithSize(MAX_SIZE, word);
    }

    public static BigInt toBigIntFromHexWithSize(int selectedSize, String word) throws Exception {
        word = word.toUpperCase();

        boolean sign;
        if (word.charAt(0) == '-') {
            sign = false;
            word = word.substring(1);
        } else if (word.charAt(0) == '+') {
            sign = true;
            word = word.substring(1);
        } else sign = true;

        // remove first 2 digits (for hex: "0x...")
        if (word.charAt(1) == 'X' && word.charAt(0) == '0')
            word = word.substring(2);

        if (word.length() < 1)
            throw new Exception("Given word has no digits.");

        // System.out.println("Reading Hex word: " + word);
        if (word.length() > MAX_SIZE) {
            throw new Exception("Given number is longer then maximum size (" + MAX_SIZE + ").");
        }

        // read string into big int
        BigInt c = new BigInt(selectedSize, HEX_DIGITS.indexOf(word.charAt(0)));
        c.spart = 1;

        int i = 1;

        // skip first zeros
        if (c.isZero()) {
            while (i < word.length() && HEX_DIGITS.indexOf(word.charAt(i)) == 0)
                i++;
        }


        for (; i < word.length(); i++) {
            int value = HEX_DIGITS.indexOf(word.charAt(i));

            BigInt digit = new BigInt(value);
            BigInt.add(BigInt.mul16(c), digit, c);
        }

        reduce(c);
        c.positive = sign;
        return c;
    }

    public static BigInt toBigIntFromOct(String word) throws Exception {
        return toBigIntFromOctWithSize(MAX_SIZE, word);
    }

    public static BigInt toBigIntFromOctWithSize(int selectedSize, String word) throws Exception {
        word = word.toUpperCase();

        boolean sign;
        if (word.charAt(0) == '-') {
            sign = false;
            word = word.substring(1);
        } else if (word.charAt(0) == '+') {
            sign = true;
            word = word.substring(1);
        } else sign = true;

        // remove first 2 digits (for hex: "0x...")
        if (word.charAt(1) == 'O' && word.charAt(0) == '0')
            word = word.substring(2);

        if (word.length() < 1)
            throw new Exception("Given word has no digits.");

        if (word.length() > MAX_SIZE) {
            throw new Exception("Given number is longer then maximum size (" + MAX_SIZE + ").");
        }

        // read string into big int
        BigInt c = new BigInt(selectedSize, charToDigit(word.charAt(0)));
        c.spart = 1;

        for (int i = 1; i < word.length(); i++) {
            BigInt digit = new BigInt(charToDigit(word.charAt(i)));
            BigInt.add(BigInt.mul8(c), digit, c);
        }

        reduce(c);
        c.positive = sign;
        return c;
    }

    // --- Print methods ---
    public String toBinary() {
        StringBuilder numberInBits = new StringBuilder();

        //if (!this.positive) numberInBits.append("-");
        //else numberInBits.append("+");

        for (int i = this.spart - 1; i >= 0; i--) {
            StringBuilder binaryCell = new StringBuilder(Integer.toBinaryString(this.values[i]));
            //fill with zeros
            while (binaryCell.length() % CELL_SIZE != 0) {
                binaryCell.insert(0, "0");
            }
            numberInBits.append(binaryCell.toString());
        }

//        to add spaces (better visibility)
        //for (int i = numberInBits.length() - 1; i > 0; i--)
        //    if (i % 8 == 0) numberInBits.insert(i, " ");

        return numberInBits.toString();
    }

    @Override
    public String toString() {
        try {
            return toDecimalString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String toDecimalString() throws Exception {
//        return "x";
        StringBuilder number = new StringBuilder();

        // so its non destructive
        BigInt tmp = (BigInt) this.clone();

        if (this.spart == 1) {
            if (!this.positive) number.append("-");
            number.append(tmp.values[0]);
            return number.toString();
        }
        else {
            while (tmp.isNotZero()) {
                int r = BigInt.divmod10(tmp);
                number.append(r);
            }
        }

        if (!this.positive) number.append("-");
        return number.reverse().toString();
    }

    public void debugString() throws Exception {
        System.out.println(
                "dec[" + this.toDecimalString() +
                        "], binary[" + toBinary() +
                        "], spart[" + this.spart +
                        "], size[" + this.size +
                        "], bitLength[" + this.spart*16 +
                        "]"
        );
    }



    // --- helper functions ---

    @Override
    public Object clone() throws CloneNotSupportedException {
        BigNumber tmp = null;
        try {
            tmp = (BigNumber) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new CloneNotSupportedException();
        }
        tmp.values = this.values.clone();
        return tmp;
    }

    public void copyFromBigInt(BigInt a) {
        this.size = a.size;
        this.spart = a.spart;
        this.positive = a.positive;
        this.values = a.values.clone();

    }

    void reduce() {
        for (int i = this.size - 1; i >= 0; i--) {
            if (this.values[i] != 0) {
                this.spart = i + 1;
                return;
            }
        }
        // at this point number is zero
        this.positive = true;
        this.spart = 1;
    }

    static void reduce(BigInt a) {
        for (int i = a.size - 1; i >= 0; i--) {
            if (a.values[i] != 0) {
                a.spart = i + 1;
                return;
            }
        }
        // at this point number is zero
        a.positive = true;
        a.spart = 1;
    }

    static void resize(BigInt a, int newSpart) throws Exception {
        // resizing bigint
        if (newSpart > a.size && a.size != MAX_SIZE) {
            a.size = MAX_SIZE;
            int[] biggerValues = new int[MAX_SIZE];
            System.arraycopy(biggerValues, 0, a.values, 0, a.values.length);
            a.values = biggerValues;
        }
        if (newSpart > MAX_SIZE) {
            throw new Exception("Resizing bigger then maximum length (" + a.size + ").");
        }
        if (a.spart > newSpart) {
            reduce(a);
            if (a.spart > newSpart) {
                throw new Exception("BigInt is larger then desired size.");
            }
        }
        if (a.spart < newSpart) {
            a.spart = newSpart;
        }
    }

    static void sameSize(BigInt a, BigInt b) throws Exception {
        if (a.spart > b.spart) {
            resize(b, a.spart);
            return;
        }
        if (b.spart > a.spart) {
            resize(a, b.spart);
        }
    }

    static SignRelation lookAtSign(BigInt a, BigInt b) {
        if (a.positive && b.positive) {
            return POSITIVE_POSITIVE;
        }
        if (!a.positive && b.positive) {
            return NEGATIVE_POSITIVE;
        }
        if (a.positive && !b.positive) {
            return POSITIVE_NEGATIVE;
        }
        return NEGATIVE_NEGATIVE;
    }

    public static int compare(BigInt a, BigInt b) {
        //cmp(a,b): a=b -->+0
        //          a>b -->+1
        //          a<b -->-1
        if (a.positive && !b.positive)
            return 1;
        if (!a.positive && b.positive)
            return -1;
        int abs = compareAbsoluteValues(a, b);
        if (!a.positive && !b.positive)
            return -abs;
        return abs;
    }

    static int compareAbsoluteValues(BigInt a, BigInt b) {
        reduce(a);
        reduce(b);
        if (a.spart > b.spart) return 1;
        if (b.spart > a.spart) return -1;

        // compare spart digits
        for (int i = a.spart - 1; i >= 0; i--) {
            if (a.values[i] > b.values[i]) return 1;
            if (b.values[i] > a.values[i]) return -1;
        }
        // at this point absolute values of numbers are equal
        return 0;
    }


    // --- additional helper functions ---

    private static int charToDigit(char value) {
        return Character.getNumericValue(value);
    }

    boolean isZero() {
        return !isNotZero();
    }
    boolean isNotZero() {
        this.reduce();
        if (this.spart > 1) return true;
        else {
            return this.values[0] > 0;
        }
    }

    public boolean isGreaterOne() {
        this.reduce();
        if (this.spart > 1) return true;
        else {
            return this.values[0] > 1;
        }
    }

    boolean isEven() {
        if ((this.values[0] & 1) == 1) {
            return false;
        }
        return true;
    }

    public boolean isOdd() {
        if (this.isEven())
            return false;
        return true;
    }

    public static boolean equals(BigInt a, BigInt b) {
        return BigInt.compare(a, b) == 0;
    }

    public static boolean greaterThen(BigInt a, BigInt b) {
        return BigInt.compare(a, b) == 1;
    }

    public static boolean lessThen(BigInt a, BigInt b) {
        return BigInt.compare(a, b) == -1;
    }
    static int zeroBits(BigInt a) {
        reduce(a);
        int bits = 0;
        for (int i = 0; i < a.spart; i++) {
            for (int j = 0; j < CELL_SIZE; j++) {
                int cell = a.values[i];
                int currentBit = (cell >> j) & 1;
                if (currentBit != 0)
                    return bits;
                bits += 1;
            }
        }
        return bits;
    }

    static int estimate(int cUpper, int cLower, int divisor) {
        long dividend = cUpper;
        if (dividend < divisor) {
            dividend = cUpper * 10 + cLower;//Todo, 10 should be base, but base is still a property and not a constant
        }
        long estimate = dividend / divisor;
        return (int) estimate;
    }


}

package com.htw.aim.its;

import com.htw.aim.its.domain.BBS;

public class BbsStatistics {
    public static void main(String[] args) throws Exception {
        //calc256with10000();
        calc65536with1000000();
    }

    public static void calc256with10000() throws Exception {
        long start = System.currentTimeMillis();
        BigInt seed = new BigInt("65537");
        int amount = 10000;
        int[] likelihood = new int[256];

        BBS bbs = new BBS(seed, 256);
        System.out.println("INFO - Creating " + amount + " random numbers with length of  bit.");
        for (int i=0; i<amount;i++) {
            if (i%1000==0)
                System.out.println("Current status: " + i + "/" + amount);
            String byty = bbs.getNextByte();
            likelihood[Integer.parseInt(byty, 2)] += 1;
            //System.out.println(i + ": " + byty + ", int: " + Integer.parseInt(byty, 2));
        }

        // print likelihood
        System.out.println("INFO - Showing all possible generated numbers (0-255) and its amount of appearance:");
        for (int i = 0; i < likelihood.length; i++) {
            if (i<10)
                System.out.print("00" + i + ": " + likelihood[i] + "\t");
            else if (i < 100)
                System.out.print("0" + i + ": " + likelihood[i] + "\t");
            else
                System.out.print(i + ": " + likelihood[i] + "\t");

            if ((i+1)%10==0)
                System.out.println();
        }


        System.out.println();
        double time = (System.currentTimeMillis() - start) / 1000.0;
        System.out.println(String.format("took %4.3fs", time));
    }

    public static void calc65536with1000000() throws Exception {
        long start = System.currentTimeMillis();
        BigInt seed = new BigInt("288424982896404711862254586374204843162197");
        int amount = 100000;
        int[] likelihood = new int[65536];

        BBS bbs = new BBS(seed, new BigInt("85142109842100096133"), new BigInt("4038430986132632737463"));
        System.out.println(bbs.getP());
        System.out.println(bbs.getQ());
        System.out.println("INFO - Creating " + amount + " random numbers with length of 16 bit.");
        for (int i=0; i<amount;i++) {
            if (i%1000==0)
                System.out.println("Current status: " + i + "/" + amount);
            String byty = bbs.getNextByte();
            byty += bbs.getNextByte();
            likelihood[Integer.parseInt(byty, 2)] += 1;
            //System.out.println(i + ": " + byty + ", int: " + Integer.parseInt(byty, 2));
        }

        // print likelihood
        System.out.println("INFO - Showing all possible generated numbers (0-65535) and it's amount of appearance:");
        for (int i = 0; i < likelihood.length; i++) {
            if (i<10)
                System.out.print("0000" + i + ": " + likelihood[i] + "\t");
            else if (i < 100)
                System.out.print("000" + i + ": " + likelihood[i] + "\t");
            else if (i < 1000)
                System.out.print("00" + i + ": " + likelihood[i] + "\t");
            else if (i < 10000)
                System.out.print("0" + i + ": " + likelihood[i] + "\t");
            else
                System.out.print(i + ": " + likelihood[i] + "\t");

            if ((i+1)%20==0)
                System.out.println();
        }

        System.out.println("last seed: " + bbs.getSeed());

        System.out.println();
        double time = (System.currentTimeMillis() - start) / 1000.0;
        System.out.println(String.format("took %4.3fs", time));
    }

}
